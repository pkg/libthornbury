/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:thornbury-model/libthornbury-model.h
 * @title: ThornburyModel
 */

#ifndef _THORNBURY_MODEL_H
#define _THORNBURY_MODEL_H

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <glib-object.h>
#include <clutter/clutter.h>
//#include <clutter-model.h>

G_BEGIN_DECLS

#define THORNBURY_TYPE_MODEL thornbury_model_get_type()

#define THORNBURY_MODEL(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  THORNBURY_TYPE_MODEL, ThornburyModel))

#define THORNBURY_MODEL_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  THORNBURY_TYPE_MODEL, ThornburyModelClass))

#define THORNBURY_IS_MODEL(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  THORNBURY_TYPE_MODEL))

#define THORNBURY_IS_MODEL_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  THORNBURY_TYPE_MODEL))

#define THORNBURY_MODEL_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  THORNBURY_TYPE_MODEL, ThornburyModelClass))

typedef struct _ThornburyModel ThornburyModel;
typedef struct _ThornburyModelClass ThornburyModelClass;
typedef struct _ThornburyModelPrivate ThornburyModelPrivate;
typedef struct _ThornburyModelIter ThornburyModelIter;
typedef struct _ThornburyModelIterClass ThornburyModelIterClass;
typedef struct _ThornburyModelIterPrivate ThornburyModelIterPrivate;

typedef ClutterModelFilterFunc ThornburyModelFilterFunc;
typedef ClutterModelSortFunc ThornburyModelSortFunc;
typedef ClutterModelForeachFunc ThornburyModelForeachFunc;

struct _ThornburyModel
{
  ClutterListModel parent;

  ThornburyModelPrivate *priv;
};

struct _ThornburyModelClass
{
  ClutterListModelClass parent_class;
};

GType thornbury_model_get_type (void) G_GNUC_CONST;

void                  thornbury_model_set_types          (ThornburyModel     *model,
                                                        guint             n_columns,
                                                        GType            *types);
void                  thornbury_model_set_names          (ThornburyModel     *model,
                                                        guint             n_columns,
                                                        const gchar * const names[]);

void                  thornbury_model_append             (ThornburyModel     *model,
                                                        ...);
void                  thornbury_model_appendv            (ThornburyModel     *model,
                                                        guint             n_columns,
                                                        guint            *columns,
                                                        GValue           *values);
void                  thornbury_model_prepend            (ThornburyModel     *model,
                                                        ...);
void                  thornbury_model_prependv           (ThornburyModel     *model,
                                                        guint             n_columns,
                                                        guint            *columns,
                                                        GValue           *values);
void                  thornbury_model_insert             (ThornburyModel     *model,
                                                        guint             row,
                                                        ...);
void                  thornbury_model_insertv            (ThornburyModel     *model,
                                                        guint             row,
                                                        guint             n_columns,
                                                        guint            *columns,
                                                        GValue           *values);
void                  thornbury_model_insert_value       (ThornburyModel     *model,
                                                        guint             row,
                                                        guint             column,
                                                        const GValue     *value);
void                  thornbury_model_remove             (ThornburyModel     *model,
                                                        guint             row);

guint                 thornbury_model_get_n_rows         (ThornburyModel     *model);
guint                 thornbury_model_get_n_columns      (ThornburyModel     *model);
const gchar *         thornbury_model_get_column_name    (ThornburyModel     *model,
                                                        guint             column);
GType                 thornbury_model_get_column_type    (ThornburyModel     *model,
                                                        guint             column);

ThornburyModelIter *    thornbury_model_get_first_iter     (ThornburyModel     *model);
ThornburyModelIter *    thornbury_model_get_last_iter      (ThornburyModel     *model);
ThornburyModelIter *    thornbury_model_get_iter_at_row    (ThornburyModel     *model,
                                                        guint             row);

void                  thornbury_model_set_sorting_column (ThornburyModel     *model,
                                                        gint              column);
gint                  thornbury_model_get_sorting_column (ThornburyModel     *model);

void                  thornbury_model_foreach            (ThornburyModel     *model,
                                                        ThornburyModelForeachFunc func,
                                                        gpointer          user_data);
void                  thornbury_model_set_sort           (ThornburyModel     *model,
                                                        gint              column,
                                                        ThornburyModelSortFunc func,
                                                        gpointer          user_data,
                                                        GDestroyNotify    notify);
void                  thornbury_model_set_filter         (ThornburyModel     *model,
                                                        ThornburyModelFilterFunc func,
                                                        gpointer          user_data,
                                                        GDestroyNotify    notify);
gboolean              thornbury_model_get_filter_set     (ThornburyModel     *model);

void                  thornbury_model_resort             (ThornburyModel     *model);
gboolean              thornbury_model_filter_row         (ThornburyModel     *model,
                                                        guint             row);
gboolean              thornbury_model_filter_iter        (ThornburyModel     *model,
                                                        ThornburyModelIter *iter);
GObject *thornbury_list_model_new (guint n_columns, ...);

GObject *thornbury_list_model_newv (guint n_columns, GType *types, const gchar *const names[]);

G_DEPRECATED_FOR (g_object_unref)
void thornbury_list_model_destroy(ThornburyModel *model);
/*
 * 
 *ThornburyModelIter
 *
 */
#define THORNBURY_TYPE_MODEL_ITER thornbury_model_iter_get_type()

#define THORNBURY_MODEL_ITER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  THORNBURY_TYPE_MODEL_ITER, ThornburyModelIter))

#define THORNBURY_MODEL_ITER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  THORNBURY_TYPE_MODEL_ITER, ThornburyModelIterClass))

#define THORNBURY_IS_MODEL_ITER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  THORNBURY_TYPE_MODEL_ITER))

#define THORNBURY_IS_MODEL_ITER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  THORNBURY_TYPE_MODEL_ITER))

#define THORNBURY_MODEL_ITER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  THORNBURY_TYPE_MODEL_ITER, ThornburyModelIterClass))


struct _ThornburyModelIter
{
  ClutterModelIter parent;

  ThornburyModelIterPrivate *priv;
};

struct _ThornburyModelIterClass
{
  ClutterModelIterClass parent_class;
};

GType thornbury_model_iter_get_type (void) G_GNUC_CONST;

void              thornbury_model_iter_get        (ThornburyModelIter *iter,
                                                 ...);
void              thornbury_model_iter_get_valist (ThornburyModelIter *iter,
                                                 va_list          args);
void              thornbury_model_iter_get_value  (ThornburyModelIter *iter,
                                                 guint             column,
                                                 GValue           *value);
void              thornbury_model_iter_set        (ThornburyModelIter *iter,
                                                 ...);
void              thornbury_model_iter_set_valist (ThornburyModelIter *iter,
                                                 va_list          args);
void              thornbury_model_iter_set_value  (ThornburyModelIter *iter,
                                                 guint             column,
                                                 const GValue     *value);

gboolean          thornbury_model_iter_is_first   (ThornburyModelIter *iter);
gboolean          thornbury_model_iter_is_last    (ThornburyModelIter *iter);
ThornburyModelIter *thornbury_model_iter_next       (ThornburyModelIter *iter);
ThornburyModelIter *thornbury_model_iter_prev       (ThornburyModelIter *iter);

ThornburyModel *    thornbury_model_iter_get_model  (ThornburyModelIter *iter);
guint             thornbury_model_iter_get_row    (ThornburyModelIter *iter);

ThornburyModelIter *thornbury_model_iter_copy       (ThornburyModelIter *iter);

G_DEPRECATED
void thornbury_model_register_column_changed_cb(ThornburyModel *model,void (*column_chnaged_cb)(gint,gpointer),gpointer userdata);

G_END_DECLS

#endif /* _THORNBURY_MODEL_H */
