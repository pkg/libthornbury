/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 *
 *
 * view_parser.c */



#include "libthornbury-viewparser.h"
#include <glib-object.h>
#include <string.h>

#define VIEW_PARSER_DEBUG_PRINT(...)		//g_print(__VA_ARGS__)
guint uinCurrentContext = 0;
gchar* pWidgetName = NULL;
enum
{
  START_VIEW_MAP = 1,
  VIEW_ID,
  VIEW_NAME,
  WIDGET_ID,
  START_WIDGET_MAP,
  WIDGET_NAME,
  START_WIDGET_ARRAY,
  WIDGET_TYPE,
  WIDGET_PROPERTIES,
  WIDGET_X_POS,
  WIDGET_Y_POS,
  WIDGET_LAYER,
  END_WIDGET_ARRAY,
  END_WIDGET_MAP,
  END_VIEW_MAP
};


static void
v_view_parser_free_style_hash_entry (gpointer key, gpointer value, gpointer data)
{
    ThornburyViewWidgets *pThornburyViewWidgetData = value;

    g_clear_pointer (&pThornburyViewWidgetData->pWidgetType, g_free);
    g_clear_pointer (&pThornburyViewWidgetData->pWidgetPropertyFilePath, g_free);
    g_clear_pointer (&key, g_free);
}

char _tmpBuffer[500];

static gint in_handle_null(void * ctx)
{
    g_printerr("API: %s: parser error\n", G_STRFUNC);
    return 1;
}

static gint in_handle_boolean(void * ctx, int boolean)
{
    g_printerr("API: %s: parser error\n", G_STRFUNC);
    return 1;
}

static gint in_handle_double(void * ctx, double dblValue)
{
    g_printerr("API: %s: parser error\n", G_STRFUNC);
    return 1;
}

static gint in_handle_integer(void * ctx, long long longValue)
{
    ThornburyView* view_data = ctx;
    ThornburyViewWidgets* pViewWidgetData;

    switch(uinCurrentContext)
    {
      case WIDGET_PROPERTIES:
      {
       uinCurrentContext = WIDGET_X_POS;
       pViewWidgetData = g_hash_table_lookup (view_data->pWidgetTable, pWidgetName);
       pViewWidgetData->inX = longValue;
       VIEW_PARSER_DEBUG_PRINT("API: handle_integer: WIDGET_X_POS %lld \n", longValue);
      }
       break;

      case WIDGET_X_POS:
      {
       uinCurrentContext = WIDGET_Y_POS;
       pViewWidgetData = g_hash_table_lookup (view_data->pWidgetTable, pWidgetName);
       pViewWidgetData->inY = longValue;
       VIEW_PARSER_DEBUG_PRINT("API: handle_integer: WIDGET_Y_POS %lld \n", longValue);
      }
       break;

      case WIDGET_Y_POS:
      {
       uinCurrentContext = WIDGET_LAYER;
       pViewWidgetData = g_hash_table_lookup (view_data->pWidgetTable, pWidgetName);
       pViewWidgetData->inLayer = longValue;
       VIEW_PARSER_DEBUG_PRINT("API: handle_integer: WIDGET_LAYER %lld \n", longValue);
      }
       break;

      default:
       g_printerr("API: handle_integer: parser error\n");
       break;
    }

    return 1;
}


static gint in_handle_string(void * ctx, const unsigned char * stringVal,
                           unsigned int stringLen)
{
  ThornburyViewWidgets* pViewWidgetData;
  ThornburyView* view_data = ctx;

  memcpy(_tmpBuffer, stringVal, stringLen);
  _tmpBuffer[stringLen] = '\0';

  switch(uinCurrentContext)
  {

    case VIEW_ID:
    {
     uinCurrentContext = VIEW_NAME;
     view_data->pViewId = g_strdup(_tmpBuffer);
     VIEW_PARSER_DEBUG_PRINT("API: handle_string: VIEW_NAME %s\n", _tmpBuffer);
    }
     break;

    case START_WIDGET_ARRAY:
    {
     uinCurrentContext = WIDGET_TYPE;
     pViewWidgetData = g_hash_table_lookup (view_data->pWidgetTable, pWidgetName);
     pViewWidgetData->pWidgetType = g_strdup(_tmpBuffer);
     VIEW_PARSER_DEBUG_PRINT("API: handle_string: WIDGET_TYPE %s\n", _tmpBuffer);
    }
     break;

    case WIDGET_TYPE:
    {
     uinCurrentContext = WIDGET_PROPERTIES;
     pViewWidgetData = g_hash_table_lookup (view_data->pWidgetTable, pWidgetName);
     pViewWidgetData->pWidgetPropertyFilePath = g_strdup(_tmpBuffer);
     VIEW_PARSER_DEBUG_PRINT("API: handle_string: WIDGET_PROPERTIES %s\n", _tmpBuffer);
    }
     break;

    default:
     g_printerr("API: handle_integer: parser error\n");
     break;
  }

    return 1;
}

static gint in_handle_map_key(void * ctx, const unsigned char * stringVal,
                            unsigned int stringLen)
{
  ThornburyViewWidgets* pViewWidgetData;
  ThornburyView* view_data = ctx;

  memcpy(_tmpBuffer, stringVal, stringLen);
  _tmpBuffer[stringLen] = '\0';

  switch(uinCurrentContext)
  {
    case START_VIEW_MAP:
     uinCurrentContext = VIEW_ID;
     VIEW_PARSER_DEBUG_PRINT("API: handle_map_key: VIEW_ID %s\n", _tmpBuffer);
     break;

    case VIEW_NAME:
     uinCurrentContext = WIDGET_ID;
     VIEW_PARSER_DEBUG_PRINT("API: handle_map_key: WIDGET_ID %s\n", _tmpBuffer);
     break;

    case START_WIDGET_MAP:
    case END_WIDGET_ARRAY:
    {
     uinCurrentContext = WIDGET_NAME;
     pViewWidgetData = g_new0(ThornburyViewWidgets , 1);
     g_hash_table_insert(view_data->pWidgetTable , (gpointer)g_strdup(_tmpBuffer), pViewWidgetData);
     pWidgetName = g_strdup(_tmpBuffer);
     VIEW_PARSER_DEBUG_PRINT("API: handle_map_key: WIDGET_NAME %s\n", _tmpBuffer);
    }
     break;
    default:
     g_printerr("API: in_handle_map_key: parser error for string %s \n" , _tmpBuffer);
     break;
  }

  return 1;
}

static gint in_handle_start_map(void * ctx)
{
  VIEW_PARSER_DEBUG_PRINT("API: handle_start_map\n");

  switch(uinCurrentContext)
  {
    case 0:
     uinCurrentContext = START_VIEW_MAP;
     VIEW_PARSER_DEBUG_PRINT("API: handle_map_key: START_VIEW_MAP %s\n", _tmpBuffer);
     break;

    case WIDGET_ID:
     uinCurrentContext = START_WIDGET_MAP;
     VIEW_PARSER_DEBUG_PRINT("API: handle_map_key: START_WIDGET_MAP %s\n", _tmpBuffer);
     break;

    default:
     g_printerr("API: handle_map_key: parser error\n");
     break;
  }

  return 1;
}


static gint in_handle_end_map(void * ctx)
{
  VIEW_PARSER_DEBUG_PRINT("API: handle_end_map\n");

  switch(uinCurrentContext)
  {
    case START_WIDGET_MAP:
    case END_WIDGET_ARRAY:
     uinCurrentContext = END_WIDGET_MAP;
     VIEW_PARSER_DEBUG_PRINT("API: handle_map_key: END_WIDGET_MAP %s\n", _tmpBuffer);
     break;

    case END_WIDGET_MAP:
     uinCurrentContext = END_VIEW_MAP;
     VIEW_PARSER_DEBUG_PRINT("API: handle_map_key: END_VIEW_MAP %s\n", _tmpBuffer);
     break;

    default:
     g_printerr("API: in_handle_end_map: parser error\n");
     break;
  }

  return 1;
}

static gint in_handle_start_array(void * ctx)
{
  VIEW_PARSER_DEBUG_PRINT("API: handle_start_array\n");
  if( uinCurrentContext == WIDGET_NAME )
  {
    uinCurrentContext = START_WIDGET_ARRAY;
  }
  else
  {
    g_printerr("API: in_handle_start_array: parser error\n");
  }
  return 1;
}

static gint in_handle_end_array(void * ctx)
{
  VIEW_PARSER_DEBUG_PRINT("API: handle_end_array\n");

  if( uinCurrentContext == WIDGET_LAYER )
  {
    uinCurrentContext = END_WIDGET_ARRAY;
  }
  else
  {
    g_printerr("API: in_handle_end_array: parser error\n");
  }
  return 1;
}

static yajl_callbacks callbacks = {
  in_handle_null,
  in_handle_boolean,
  in_handle_integer,
  in_handle_double,
  NULL, // handle_number
  in_handle_string,
  in_handle_start_map,
  in_handle_map_key,
  in_handle_end_map,
  in_handle_start_array,
  in_handle_end_array
};



/**
 * thornbury_view_parser_free_view:
 * @viewInfo: The view information containing all the widget details.
 *
 * Frees all the view manifest information for a particular view.
 *
 * Returns: void.
 */
void
thornbury_view_parser_free_view( ThornburyView* viewInfo )
{
	if(NULL != viewInfo)
	{
	  if(viewInfo->pViewId)
	  {
	    g_free( viewInfo->pViewId );
	    viewInfo->pViewId = NULL;
	  }

	  if(viewInfo->pWidgetTable)
	  {
		  g_hash_table_foreach(viewInfo->pWidgetTable, v_view_parser_free_style_hash_entry, NULL);
		  viewInfo->pWidgetTable = NULL;
	  }
	}
}

/**
 * thornbury_view_parser_parse_file:
 * @pFile: The view information containing all the widget details
 *
 * Creates the hash table of all the widgets in a view.
 *
 * Returns: ThornburyView pointer .
 */
ThornburyView*
thornbury_view_parser_parse_file(gchar *pFile )
{
  ThornburyView* view_data = g_new0(ThornburyView , 1);
  uinCurrentContext = 0;
  view_data->pWidgetTable = g_hash_table_new (g_str_hash, g_str_equal);
  if (ThornburyParseJsonFile( pFile, &callbacks , view_data ) != THORNBURY_SUCCESS)
    {
      g_printerr("API: %s: parser error\n", G_STRFUNC);
    }

  return view_data;
}

/**
 * thornbury_view_parser_parse_resource:
 * @path:Gresource path of view json file containing all the widget details
 * @error: return location for a #GError, or NULL
 *
 * Create a new #ThornburyView from a JSON view file which is stored in the
 * global GResource namespace. See the #GResource documentation for information
 * about the naming of files in the GResource namespace — this function
 * effectively calls g_resources_lookup_data() on the given @path.
 *
 * The JSON view file must be in the correct format described in the [view
 * format](https://git.staging.apertis.org/cgit/frampton.git/tree/resources/views/songs/DetailView.json)
 *
 * If the @path refers to a non-existent file, %G_RESOURCE_ERROR_NOT_FOUND will
 * be returned. If the file could be found, but the JSON data could not be
 * parsed or was in an invalid format, %G_RESOURCE_ERROR_INTERNAL will be returned.
 *
 * Returns: (transfer full): a new #ThornburyView, or %NULL on error
 * Since: UNRELEASED
 */
ThornburyView*
thornbury_view_parser_parse_resource (const gchar *path,
                                      GError **error)
{
  ThornburyView *view_data = NULL;
  GError *tmp_error = NULL;

  g_return_val_if_fail (path != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  view_data = g_new0 (ThornburyView, 1);
  view_data->pWidgetTable = g_hash_table_new (g_str_hash, g_str_equal);
  ThornburyParseJsonResource (path, &callbacks, view_data, &tmp_error);

  if (tmp_error != NULL)
    {
      g_free (view_data);
      g_debug ("VIEWPARSER: error %s", tmp_error->message);
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  uinCurrentContext = 0;

  return view_data;
}

