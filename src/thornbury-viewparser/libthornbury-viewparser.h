/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:thornbury-viewparser/libthornbury-viewparser.h
 * @title: ThornburyViewParser
 * @short_description: Thornbury View Parser to parse the view JSON files.
 *
 * The view definition describes the widgets that go into a particular view
 * of an application. Each view is defined in a separate JSON file.
 *
 * The view definition needs to be defined per skin, and if a project supports
 * more than one skin then the app developer needs to generate separate view
 * JSON files.
 *
 * The view definition basically expects a unique widget name for that
 * application, its widget con-figuration path and the layering information
 * for that particular widget.
 *
 * The view definition parser would parse these xml and feed it to the view
 * manager. The view manager would then construct the view based on the
 * information provided.
 *
 * It would iteratively call the widget initialization calls to start the
 * widget creation process. It also takes care of the layering of the various
 * widgets within the view.
 *
 * If an app developer doesn't intend to use the view manager, then he needs
 * to parse this JSON, call the corresponding widget creation call and create
 * his own views.
 *
 * Using #ThornburyView class a special parser library will be available in
 * the SDK to parse the entries in this file and feed the properties to
 * the #ThornburyViewManager.
 *
 * Sample View JSON
 *
 * ```C
 * {
 *   "viewId":"SampleView",
 *   "widgets":{
 *   "Roller": ["RollerContainer", "Sample_View_Roller_prop", 0, 5, 1],
 *              "SortRoller": ["RollerContainer","Sample_View_SortRoller_prop",660,0,2],
 *                             "ViewsDrawer":["ViewsDrawer","Sample_View_ViewsDrawer_prop", 660, 51, 3],
 *                                            "ContextDrawer":["ContextDrawer","Sample_View_ContextDrawer_prop", 660, 394, 5],
 *                                            "Overlay":["Overlay", "Sample_View_Overlay_prop", 0, 0, 4]
 *             }
 * }
 * ```
 *
 * ViewParser is available since thornbury-base 1.0
 */

#ifndef _VIEW_PARSER_H
#define _VIEW_PARSER_H

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include "libthornbury-jsonparser.h"
#include <glib-object.h>
#include <string.h>


G_BEGIN_DECLS


typedef struct _ThornburyView ThornburyView;
typedef struct _ThornburyViewWidgets ThornburyViewWidgets;

/**
 * ThornburyView:
 * @pViewId: Name of the view.
 * @pWidgetTable: A table of all teh widgets in the view.
 *
 * Struct for the gobject type.
 */
struct _ThornburyView
{
	gchar* pViewId;    /* name of the view */
	GHashTable *pWidgetTable;  /* a table of all the widgets in the view */
};

/**
 * ThornburyViewWidgets:
 * @pWidgetType: Widget Type.
 * @pWidgetPropertyFilePath: Widget's property JSON file.
 * @inX: X position of the widget.
 * @inY: Y position of the widget.
 * inLayer: View Layer of the widget.
 *
 * Struct for the gobject type.
 */
struct _ThornburyViewWidgets
{
	gchar* pWidgetType;             /* widget type */
	gchar* pWidgetPropertyFilePath; /* widget's property json file */
	gint   inX;	                    /* X position of the widget */
	gint   inY;	                    /* Y position of the widget */
	guint  inLayer;                 /* View Layer of the widget */
};

ThornburyView*
thornbury_view_parser_parse_file (gchar *file_path );

ThornburyView *thornbury_view_parser_parse_resource (const gchar *file_path,
                                                     GError **error);

void
thornbury_view_parser_free_view( ThornburyView* viewInfo );


G_END_DECLS

#endif /* _VIEW_PARSER_H */
