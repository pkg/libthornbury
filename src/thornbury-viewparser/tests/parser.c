/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/* Copyright (c) 2015-2016 Collabora */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <glib.h>
#include <gio/gio.h>

#include "libthornbury-viewparser.h"

/* Generated file defines @json_instances. */
#include "parser-vectors.h"

static void
test_parser (gconstpointer user_data)
{
  guint i;
  GFile *test_file = NULL;
  GFileIOStream *io_stream = NULL;
  GOutputStream *output_stream;
  ThornburyView *result = NULL;
  gchar *test_file_path = NULL;
  GError *error = NULL;

  i = GPOINTER_TO_UINT (user_data);

  /* Run the test in a subprocess to handle parser crashes and catch stderr. */
  if (!g_test_subprocess ())
    {
      g_test_trap_subprocess (NULL, 0, 0);

      if (json_instances[i].is_valid)
        g_test_trap_assert_stderr ("");
      else
        g_test_trap_assert_stderr ("*parser error*");

      g_test_trap_assert_passed ();

      return;
    }

  /* Dump the test vector out to a temporary file for the parser. */
  test_file = g_file_new_tmp ("view-parser-XXXXXX.json",
                              &io_stream, &error);
  g_assert_no_error (error);

  output_stream = g_io_stream_get_output_stream (G_IO_STREAM (io_stream));
  g_output_stream_write_all (output_stream, json_instances[i].json,
                             json_instances[i].size, NULL, NULL, &error);
  g_assert_no_error (error);

  g_io_stream_close (G_IO_STREAM (io_stream), NULL, &error);
  g_assert_no_error (error);

  test_file_path = g_file_get_path (test_file);

  /* Try parsing the file. Note that it will always return success, even if it
   * completely failed to parse the JSON. */
  result = thornbury_view_parser_parse_file (test_file_path);
  g_clear_pointer (&result, thornbury_view_parser_free_view);

  /* Tidy up. */
  g_file_delete (test_file, NULL, &error);
  g_assert_no_error (error);

  g_free (test_file_path);
  g_clear_object (&io_stream);
  g_clear_object (&test_file);
}

int
main (int    argc,
      char **argv)
{
  guint i;

  g_test_init (&argc, &argv, NULL);

  for (i = 0; i < G_N_ELEMENTS (json_instances); i++)
    {
      gchar *test_name = NULL;

      test_name = g_strdup_printf ("/libthornbury/view-parser/parser/%u", i);
      g_test_add_data_func (test_name, GUINT_TO_POINTER (i), test_parser);
      g_free (test_name);
    }

  return g_test_run ();
}
