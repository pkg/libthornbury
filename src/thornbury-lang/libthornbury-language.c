/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


 /* thornbury-language.c */



#include <stdlib.h>
#include <stdio.h>
#include <locale.h>

#include "libthornbury-language.h"

gchar* pBaseFolderPath = NULL;
/**
 * thornbury_lang_configure_AppData:
 * @rLangConfigData: path related to language file path
 */

void thornbury_lang_configure_AppData (ThornburyApp_LangConfigData *rLangConfigData)
{
    g_return_if_fail (rLangConfigData != NULL);
    g_return_if_fail (rLangConfigData->pBaseFolderPath != NULL);
    g_return_if_fail (rLangConfigData->pBaseFileName != NULL);

    //bind the base-folder-path where mo-file for all locales are placed.
    bindtextdomain( rLangConfigData->pBaseFileName,
                    rLangConfigData->pBaseFolderPath);
    textdomain(rLangConfigData->pBaseFileName);

    pBaseFolderPath = g_strdup(rLangConfigData->pBaseFolderPath);
}
static void lang_update_AppData (ThornburyApp_LangConfigData *rLangConfigData)
{
    g_return_if_fail (rLangConfigData != NULL);
    g_return_if_fail (rLangConfigData->pBaseFolderPath != NULL);
    g_return_if_fail (rLangConfigData->pBaseFileName != NULL);

    //bind the base-folder-path where mo-file for all locales are placed.
    bindtextdomain( rLangConfigData->pBaseFileName,
                    rLangConfigData->pBaseFolderPath);
    textdomain(rLangConfigData->pBaseFileName);
}

/**
 * lang_set_current_language:
 * @pNewLanguage: Name of the language
 * @pNewLocale: Name of the Locale.
 */

void thornbury_lang_set_current_language (const gchar *pNewLanguage,
                                const gchar *pNewLocale)
{
    g_return_if_fail (pNewLanguage != NULL);
    g_return_if_fail (pNewLocale != NULL);

    //set the Language and locale envirnoment variables.
    setenv ("LANGUAGE", pNewLanguage, TRUE);
    setlocale (LC_ALL, pNewLocale);

    //Subsequent gettext() calls returns strings from new language/mo-file.
    //NOTE: All text fields in UI-Application must be refreshed...
}
/**
 * get_text:
 * @pText: text to be translated.
 *
 */


gchar* thornbury_get_text( gchar* pText )
{
  //always first check for a string in application folder
  gchar* pTranslatedText = _(pText);
  if( g_strcmp0( pTranslatedText , pText ) == FALSE )
  {
    //if string not found in app folder then search in global folder
    const char *pEnvString;
    pEnvString = g_getenv ("GLOBAL_LANGUAGE_STRINGS");
    if (pEnvString != NULL)
    {
      //set the language path to global folder
      ThornburyApp_LangConfigData rAppData;
      rAppData.pBaseFolderPath = g_strdup(pEnvString);
      rAppData.pBaseFileName = g_strdup ("language");
      lang_update_AppData(&rAppData);
      g_clear_pointer (&rAppData.pBaseFileName, g_free);
      g_clear_pointer (&rAppData.pBaseFolderPath, g_free);
      pTranslatedText = _(pText);
      //reset the language path to application folder
      rAppData.pBaseFolderPath = g_strdup(pBaseFolderPath);
      rAppData.pBaseFileName = g_strdup ("language");
      lang_update_AppData(&rAppData);
      g_clear_pointer (&rAppData.pBaseFileName, g_free);
      g_clear_pointer (&rAppData.pBaseFolderPath, g_free);
    }
  }
  return pTranslatedText;
}

