/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* web_texture.c */


#include "libthornbury-webtexture.h"

#define WEB_TEXTURE_DEBUG(...)  //g_print( __VA_ARGS__)

typedef struct _WebAsyncData WebAsyncData;

static GQuark thornbury_web_texture_error_quark(void);
static gboolean web_texture_cb (gpointer data);
static gboolean web_pixbuf_content_cb (gpointer data);
static gboolean web_pixbuf_set_content_cb (gpointer data);

#define THORNBURY_WEB_TEXTURE_ERROR (thornbury_web_texture_error_quark())

static GHashTable* appInfohash = NULL;

static GQuark
thornbury_web_texture_error_quark (void)
{
	return g_quark_from_static_string("thornbury_web_texture_error_quark");
}

/* thread data */
struct _WebAsyncData
{
	gfloat flWidth;
	gfloat flHeight;
	gchar *pFilePath;
	GError **pErr;
	gpointer pUserData;
	GdkPixbuf *pixbuf;
	ClutterActor *pBox;
	gboolean setData;
	web_image_created_cb image_cb; //void (*web_image_created_cb) (ClutterActor *pimage, GError *pError, gpointer pUserData);
	web_image_content_created_cb content_image_cb; //void (*web_image_content_created_cb) (ClutterContent *pimage, GError *pError, gpointer pUserData);
};


typedef struct _curlIOHTTPRead
{
	char *buffer;
	size_t sizeBuffer;
} curlIOHTTPRead;

/********************************************************
 * Function : ui_texture_write_image_data_buffer
 * Description: using curl write image data
 * Parameters:  vPtr, size, nmemb, vStream*
 * Return value: SoupMessage*
 ********************************************************/
static size_t ui_texture_write_image_data_buffer (void *vPtr, size_t size, size_t nmemb, void *vStream)
{
	    curlIOHTTPRead *context = (curlIOHTTPRead*) vStream;
        if (context->buffer == NULL)
        {
        	context->buffer = (gchar *) malloc (size * nmemb);
                memcpy (context->buffer, vPtr, size * nmemb);
        } else
        {
        	context->buffer = (gchar *) realloc (context->buffer, context->sizeBuffer + (size * nmemb));
                memcpy (context->buffer + context->sizeBuffer, vPtr, size * nmemb);
        }
        context->sizeBuffer += (size * nmemb);

        return (size * nmemb);
}

/********************************************************
 * function : web_texture_download_file
 * description: download url using curl
 * parameters:  puri*, pData
 * return value: GdkPixbuf*
 ********************************************************/
static GdkPixbuf *web_texture_download_file (const gchar *pUri, gpointer pData)
{
	GError *err = NULL;
	CURL *cpUrl;
	CURLcode res;
	GdkPixbuf *pixbuf = NULL;
	GdkPixbufLoader *pLoader;
	curlIOHTTPRead* context;

	context = (curlIOHTTPRead*) malloc(sizeof(curlIOHTTPRead));
	if (context == NULL)
	{
		g_warning("memory creation error");
		return (NULL);
	}

	/*Use CpUrl to fetch data */
	cpUrl = curl_easy_init ();
	if (cpUrl)
	{
		context->buffer = NULL;
		context->sizeBuffer = 0;

		curl_easy_setopt(cpUrl, CURLOPT_URL, pUri);
		curl_easy_setopt(cpUrl, CURLOPT_WRITEFUNCTION, ui_texture_write_image_data_buffer);
		curl_easy_setopt(cpUrl, CURLOPT_WRITEDATA, context);


		res = curl_easy_perform (cpUrl);
		curl_easy_cleanup (cpUrl);
		if (res != 0 ||context->buffer == NULL)
		{
			g_warning ("%s: CpUrl perform failed -- %s\n", __FUNCTION__, curl_easy_strerror (res));
			return NULL;
		}
	}
	else
	{
		g_warning ("%s: CpUrl init failed\n", __FUNCTION__);
		return NULL;
	}

	pLoader = gdk_pixbuf_loader_new ();
	if (gdk_pixbuf_loader_write (pLoader, (const guchar *)context->buffer, context->sizeBuffer, &err) == FALSE)
	{
		if(NULL != err)
		{
			g_warning ("%s %d: %s\n", __FUNCTION__, __LINE__, err->message);
			gdk_pixbuf_loader_close (pLoader, NULL);
			if(G_IS_OBJECT(pLoader))
			{
				g_object_unref(pLoader);
				pLoader = NULL;
			}
			return NULL;
		}
	}
	else
	{
		if (gdk_pixbuf_loader_close (pLoader, &err) == FALSE)
		{
			if(NULL != err)
			{
				g_warning ("%s %d: %s\n", __FUNCTION__, __LINE__, err->message);
				if(G_IS_OBJECT(pLoader))
				{
					g_object_unref(pLoader);
				}
				return NULL;
			}
		}
		else
		{
			pixbuf = g_object_ref (gdk_pixbuf_loader_get_pixbuf (pLoader));
			if (pixbuf == NULL)
			{
				if(G_IS_OBJECT(pLoader))
					g_object_unref(pLoader);
				return NULL;
			}
		}
	}
	context->sizeBuffer = 0;
	if(NULL != context->buffer)
	{
		free (context->buffer);
		context->buffer = NULL;
	}
	if(NULL != context)
	{
		free (context);
		context = NULL;
	}
	return pixbuf;
}

static void web_set_content_data(ClutterContent *pImage, GdkPixbuf *pixbuf, GError **pErr)
{
	clutter_image_set_data(CLUTTER_IMAGE (pImage),
			gdk_pixbuf_get_pixels (pixbuf),
			gdk_pixbuf_get_has_alpha (pixbuf)? COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888,
			gdk_pixbuf_get_width (pixbuf),
			gdk_pixbuf_get_height (pixbuf),
			gdk_pixbuf_get_rowstride (pixbuf),
			pErr);
}

static gboolean
web_texture_cb (gpointer pData)
{
	WebAsyncData *pThreadData = (WebAsyncData *)pData;

	ClutterContent *pImage = NULL;
	ClutterActor *pBox = NULL;
	GError *err = NULL;

	if(NULL != pThreadData->pixbuf) //&& NULL == *(pThreadData->pErr) )
	{
		/* create content */
		pImage = clutter_image_new ();
		web_set_content_data(pImage, pThreadData->pixbuf, pThreadData->pErr);
		if(NULL == *(pThreadData->pErr))
		{
			/* set content for the box */
			pBox = clutter_actor_new ();
			clutter_actor_set_content (pBox, pImage);
			clutter_actor_set_size (pBox, gdk_pixbuf_get_width (pThreadData->pixbuf), gdk_pixbuf_get_height (pThreadData->pixbuf));
		}
	}
	/* free pixbuf */
	if(G_IS_OBJECT(pThreadData->pixbuf))
	{
		g_object_unref (pThreadData->pixbuf);
		pThreadData->pixbuf = NULL;
	}
	/* free file path */
	if(NULL != pThreadData->pFilePath)
	{
		g_free(pThreadData->pFilePath);
		pThreadData->pFilePath = NULL;
		WEB_TEXTURE_DEBUG("free path\n");
	}
	/* callback */
	pThreadData->image_cb(pBox, err, pThreadData->pUserData);
	if(NULL != *(pThreadData->pErr))
	{
		g_error_free(*(pThreadData->pErr));
		*(pThreadData->pErr) = NULL;
	}
	/* free thread struct pointer */
	if(NULL != pThreadData)
	{
		g_free(pThreadData);
		pThreadData = NULL;
	}
	/* return from timeout */
	return FALSE;
}

static void create_web_texture_thread(gpointer pData)
{
	WebAsyncData *pThreadData = (WebAsyncData *)pData;
	WEB_TEXTURE_DEBUG("In Thread create pixbuf \n");

	pThreadData->pixbuf = web_texture_download_file(pThreadData->pFilePath,pThreadData);

	if(NULL == pThreadData->pixbuf)
	{
		g_warning("error in image pixbuf creation \n");
		return;
	}
	if (pThreadData->flWidth != 0 && pThreadData->flHeight != 0)
	{
		pThreadData->pixbuf = gdk_pixbuf_scale_simple (pThreadData->pixbuf, pThreadData->flWidth, pThreadData->flHeight, GDK_INTERP_BILINEAR);
	}
	/* come out of thread to load content data */
	g_timeout_add(10, (GSourceFunc)web_texture_cb, pThreadData);
}
/**
 * thornbury_web_texture_create_async:
 * @pFilePath:  http Path of the image
 * @flWidth: width of the image to be created.
 * @flHeight: height of the image to be created.
 * @pData:data to the callback.
 * @pFunCB:function pointer of the claaback of type #web_image_created_cb.
 *
 * This function creates the image asynchronous, hence it is not a blocking call.
 * after creating the image it will call the callback function with the content.
 */


void thornbury_web_texture_create_async (gchar *pFilePath, gfloat flWidth, gfloat flHeight, gpointer pData, web_image_created_cb pFunc)
{
	static GError *pErr = NULL;
	if(NULL != pErr)
	{
		g_clear_error(&pErr);
		pErr = NULL;
	}
	if(NULL != pFilePath)
	{
		WebAsyncData *pThreadData = g_new0(WebAsyncData, 1);
		pThreadData->pFilePath = g_strdup(pFilePath);
		pThreadData->flWidth = flWidth;
		pThreadData->flHeight = flHeight;
		pThreadData->image_cb = pFunc;
		pThreadData->pUserData = pData;
		pThreadData->pErr = &pErr;
		pThreadData->pixbuf = NULL;
		g_thread_new("web-texture-thread", (GThreadFunc)create_web_texture_thread, pThreadData);
	}
}

static gboolean
web_pixbuf_content_cb (gpointer pData)
{
	WebAsyncData *pThreadData = (WebAsyncData *)pData;
	GError *err = NULL;
	ClutterContent *pImage = NULL;

	if(NULL != pThreadData->pixbuf && NULL == *(pThreadData->pErr))
	{
		/* create content */
		pImage = clutter_image_new ();
		web_set_content_data(pImage, pThreadData->pixbuf, pThreadData->pErr);
	}
	/* free pixbuf */
	if(G_IS_OBJECT(pThreadData->pixbuf))
	{
		g_object_unref (pThreadData->pixbuf);
		pThreadData->pixbuf = NULL;
	}
	/* free file path */
	if(NULL != pThreadData->pFilePath)
	{
		g_free(pThreadData->pFilePath);
		pThreadData->pFilePath = NULL;
		WEB_TEXTURE_DEBUG("free path\n");
	}
	if(NULL != *(pThreadData->pErr))
	{
		g_set_error_literal(&err, THORNBURY_WEB_TEXTURE_ERROR, 0, (*(pThreadData->pErr))->message);
	}
	/* callback */
	pThreadData->content_image_cb(pImage, err, pThreadData->pUserData);

	if(NULL != *(pThreadData->pErr))
	{
		g_error_free(*(pThreadData->pErr));
		*(pThreadData->pErr) = NULL;
	}
	/* free thread struct pointer */
	if(NULL != pThreadData)
	{
		g_free(pThreadData);
		pThreadData = NULL;
	}
	/* return from timeout */
	return FALSE;
}

static void create_web_texture_content_thread(gpointer pData)
{
	WebAsyncData *pThreadData = (WebAsyncData *)pData;
	WEB_TEXTURE_DEBUG("In Thread create pixbuf \n");
	pThreadData->pixbuf = web_texture_download_file(pThreadData->pFilePath, pThreadData);

	if(NULL == pThreadData->pixbuf)
	{
		g_warning("error in image pixbuf creation \n");
		return;
	}
	if (pThreadData->flWidth != 0 && pThreadData->flHeight != 0)
	{
		pThreadData->pixbuf = gdk_pixbuf_scale_simple (pThreadData->pixbuf, pThreadData->flWidth, pThreadData->flHeight, GDK_INTERP_BILINEAR);
	}
	/* come out of thread to load content data */
	g_timeout_add(10, (GSourceFunc)web_pixbuf_content_cb, pThreadData);
}
/**
 * thornbury_web_texture_create_content_async:
 * @pFilePath:  http Path of the image
 * @flWidth: width of the image to be created.
 * @flHeight: height of the image to be created.
 * @pData:data to the callback.
 * @pFunc:function pointer of the callback of type #web_image_content_created_cb.
 *
 * This function creates the content asynchronous, hence it is not a blocking call.
 * after creating the content it will call the callback function with the content.
 */

void thornbury_web_texture_create_content_async (gchar *pFilePath, gfloat flWidth, gfloat flHeight, gpointer pData, web_image_content_created_cb pFunc)
{
	static GError *pErr = NULL;
	if(NULL != pErr)
	{
		g_clear_error(&pErr);
		pErr = NULL;
	}
	if(NULL != pFilePath)
	{
		WebAsyncData *pThreadData = g_new0(WebAsyncData, 1);
		pThreadData->pFilePath = g_strdup(pFilePath);
		pThreadData->flWidth = flWidth;
		pThreadData->flHeight = flHeight;
		pThreadData->content_image_cb = pFunc;
		pThreadData->pUserData = pData;
		pThreadData->pErr = &pErr;
		pThreadData->pixbuf = NULL;
		g_thread_new("web-content-texture-thread", (GThreadFunc)create_web_texture_content_thread, pThreadData);
	}
}

static void v_iter_hash(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	WebAsyncData *pThreadData = (WebAsyncData *)pUserData;
	//g_print("###%s### ###%s###\n",(gchar*)pValue,pThreadData->pFilePath);

	if(pThreadData->pBox == (ClutterActor*)pKey)
	{
		if( g_strcmp0(pValue,pThreadData->pFilePath) == 0)
		{
			//g_print("value has found in hash list so remove from list \n");
			g_hash_table_remove(appInfohash,(ClutterActor*)pKey);
			pThreadData->setData = FALSE;
		}
	}
}

static gboolean
web_pixbuf_set_content_cb (gpointer pData)
{
	WebAsyncData *pThreadData = (WebAsyncData *)pData;
	if(appInfohash)
	{
		g_hash_table_foreach(appInfohash, v_iter_hash, pThreadData);
	}

	if(( NULL != pThreadData->pBox ) && (pThreadData->setData == TRUE) )
	{
		ClutterContent *pImage = clutter_actor_get_content(pThreadData->pBox);
		if(NULL != pThreadData->pixbuf)// && (NULL == pThreadData->pErr && NULL == *(pThreadData->pErr)))
		{
			web_set_content_data(pImage, pThreadData->pixbuf, pThreadData->pErr);
		}
	}
	else
	{
		if( NULL != pThreadData->pErr)
		{
			/* emit error signal */
			g_set_error_literal(pThreadData->pErr, THORNBURY_WEB_TEXTURE_ERROR, 0, "Clutter Actor is Null");
			return FALSE;
		}
		else
		{
			g_warning("Clutter Actor is Null\n");
			return FALSE;
		}
	}
#if 1
	/* free pixbuf */
	if(G_IS_OBJECT(pThreadData->pixbuf))
	{
		g_object_unref (pThreadData->pixbuf);
		pThreadData->pixbuf = NULL;
	}
	/* free file path */
	if(NULL != pThreadData->pFilePath)
	{
		g_free(pThreadData->pFilePath);
		pThreadData->pFilePath = NULL;
		WEB_TEXTURE_DEBUG("free path\n");
	}
	/* free thread struct pointer */
	if(NULL != pThreadData)
	{
		g_free(pThreadData);
		pThreadData = NULL;
	}
#endif
	return FALSE;
}

static void create_set_web_texture_thread(gpointer pData)
{
	WebAsyncData *pThreadData = (WebAsyncData *)pData;
	WEB_TEXTURE_DEBUG("create_set_web_texture_thread \n");

	if(NULL == pThreadData->pFilePath)
	{
		if( NULL != pThreadData->pErr)
		{
			/* emit error signal */
			g_set_error_literal(pThreadData->pErr, THORNBURY_WEB_TEXTURE_ERROR, 0, "File not found");
			return ;
		}
		else
		{
			g_warning("File not found\n");
			return ;
		}
	}
	pThreadData->pixbuf = web_texture_download_file(pThreadData->pFilePath,pData);

	if(NULL == pThreadData->pixbuf)
	{
		g_warning("error in image pixbuf creation \n");
		return;
	}

	if (pThreadData->flWidth != 0 && pThreadData->flHeight != 0)
	{
		pThreadData->pixbuf = gdk_pixbuf_scale_simple (pThreadData->pixbuf, pThreadData->flWidth, pThreadData->flHeight, GDK_INTERP_BILINEAR);
	}

	/* come out of thread to load content data */
	g_timeout_add(10, (GSourceFunc)web_pixbuf_set_content_cb, pThreadData);
}
/**
 * thornbury_web_texture_set_from_file:
 * @pBox:container to which image has to get set.
 * @pFilePath:  http Path of the image
 * @flWidth: width of the image to be created.
 * @flHeight: height of the image to be created.
 * @pErr: error data set in the funtion.
 *
 * This function creates the content asynchronous, hence it is not a blocking call.
 * after creating the content it will add to the container.If fails pErr will have error message.
 */



void thornbury_web_texture_set_from_file (ClutterActor *pBox, gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError **pErr)
{
	WEB_TEXTURE_DEBUG("%s\n", __FUNCTION__);

	/* if image actor or file path is null, return */
	if ( pBox == NULL || pFilePath == NULL)
	{
		if(NULL != pErr)
		{
			//g_warning ("%s: Path/Image Actor is NULL\n", __FUNCTION__);
			g_set_error_literal(pErr, THORNBURY_WEB_TEXTURE_ERROR, 0, "Path/Image Actor is NULL");
			return;
		}
		else
		{
			g_warning ("%s: Path/Image Actor is NULL\n", __FUNCTION__);
			return;
		}
	}

	if(NULL != pFilePath && NULL != pBox)
	{
		WebAsyncData *pThreadData = g_new0(WebAsyncData, 1);
		pThreadData->pFilePath = g_strdup(pFilePath);
		pThreadData->flWidth = flWidth;
		pThreadData->flHeight = flHeight;
		pThreadData->content_image_cb = NULL;
		pThreadData->pErr = pErr;
		pThreadData->pixbuf = NULL;
		pThreadData->pBox = pBox;
		pThreadData->setData = TRUE;
		g_thread_new("web-content-texture-thread", (GThreadFunc)create_set_web_texture_thread, pThreadData);
	}
}

/**
 * thornbury_web_texture_cancel_request:
 * @pBox:container to which image has to get set.
 * @pFilePath:  http Path of the image
 *
 */


void thornbury_web_texture_cancel_request(ClutterActor *pBox, gchar *pFilePath)
{
	if((NULL == pBox) || (NULL == pFilePath) )
	{
		g_warning(" In %s Filepath or Actor is null \n ",__FUNCTION__);
		return;
	}
	if(NULL == appInfohash)
	{
		//g_print("hash is null so create an hash list \n");
		appInfohash = g_hash_table_new(g_direct_hash, g_str_equal );
		g_hash_table_insert(appInfohash, pBox, pFilePath);
	}
	else
	{
		//g_print("hash is not null so insert it into an hash list \n");
		g_hash_table_insert(appInfohash, pBox, pFilePath);
	}
}
