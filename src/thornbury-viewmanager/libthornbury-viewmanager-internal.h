/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __VIEW_MANAGER_INTERNAL_H__
#define __VIEW_MANAGER_INTERNAL_H__

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include "libthornbury-viewmanager.h"
#include "libthornbury-texture.h"
#include <gdk-pixbuf/gdk-pixdata.h>


G_BEGIN_DECLS

/* FIXME: Thornbury should use common logging macro */
/*< private >*/
extern guint  view_manager_debug_flags;
enum _ThornburyViewManagerDebugFlag
{
	VIEW_MANAGER_DEBUG = 1 << 0,

};

/* Set the environment variable in terminal to enable traces: export VIEW_MANAGER_DEBUG=view-manager */
/*< private >*/
#define VIEW_MANAGER_HAS_DEBUG               ((view_manager_debug_flags ) & 1)
/*< private >*/
#define VIEW_MANAGER_PRINT( a ...) \
 	if (G_LIKELY (VIEW_MANAGER_HAS_DEBUG )) \
	{                            	\
	       	g_print(a);           \
	}

static const GDebugKey view_manager_debug_keys[] =
{
	{ "view-manager",   VIEW_MANAGER_DEBUG }
};


#define strlayer "layer"
#define strwidgetpath "widgets/"
#define strJSON ".json"

#define DISABLE_BUILD_BY_NAME 1

typedef struct _ThornburyViewManagerGlobalWidgets ThornburyViewManagerGlobalWidgets;

#define VM_DBKEY_VIEW_ORDER "ViewOrder"
#define VM_DBKEY_VIEW_NAME "ViewName"

#define VM_APPDATA_DBKEY_KEY "Key"
#define VM_APPDATA_DBKEY_VALUE "Value"

#define VM_DBKEY_VIEW_STACK_TABLE "ViewStack"
#define VM_DBKEY_APP_DATA_TABLE "AppData"

struct _ThornburyViewManagerGlobalWidgets
{
	ClutterActor *widget;
	gchar *current_view;
};

#define VIEW_MANAGER_PDI_SUFFIX "_view_stack"

//void v_view_manager_switch_view_internal_call(ThornburyViewManager *view_manager,gchar *view_to_switch,
//		gboolean addtohistory,guint iSelected_row , gboolean bInternal);

gchar *p_thornbury_view_manager_restore_any_non_default_view (ThornburyViewManager *view_manager,
                                                              GList **view_history);

void v_thornbury_view_manager_create_application_pdi(gchar *pAppName);

GHashTable *p_thornbury_view_manager_read_app_data_from_db (void);

void v_thornbury_view_manager_take_app_screenshot (void) ;

#if DISABLE_BUILD_BY_NAME
void v_view_manager_map_gloabl_widgets(ThornburyViewManagerViewData * viewdata);
#endif


G_END_DECLS
#endif
