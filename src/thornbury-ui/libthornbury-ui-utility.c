/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*  libthornbury-ui-utility.c
 *
 */

#include "libthornbury-ui-utility.h"
#include <clutter/clutter.h>

/***********************************************************************************
        @Macro Definitions
************************************************************************************/
#define UI_UTILITY_DEBUG(...)   //g_print( __VA_ARGS__)

/************************************************************************************
 * Description of Fixes/Functionality:
-----------------------------------------------------------------------------------
        Description                             Date                    Name
        ----------                              ----                    ----


************************************************************************************/

typedef GType (*GTypeGetFunc) (void);

/********************************************************
 * function : get_type_from_name
 * description: to get GType of the GObject 
 * parameters:  pName of the GObject
 * return value: GType
 ********************************************************/
static GType get_type_from_name (const gchar *pName)
{
        static GModule *pModule = NULL;

        g_autoptr (GString) buffer = g_string_sized_new (64);
        GType gtype = G_TYPE_INVALID;
        GTypeGetFunc function;
        gchar *pSymbol;
        gint inIndex = 0;

        gtype = g_type_from_name (pName);
        if (gtype != G_TYPE_INVALID)
                return gtype;

        if (G_UNLIKELY (!pModule))
                pModule = g_module_open (NULL, G_MODULE_BIND_LAZY | G_MODULE_BIND_LOCAL);

		while(pName[inIndex] != '\0')
        {
                gchar chr = pName[inIndex];
				gchar chrUp = g_ascii_toupper (chr);
				gchar chrUpPrev = g_ascii_toupper (pName[inIndex - 1]);
				gchar chrUpPrev2 = g_ascii_toupper (pName[inIndex - 2]);
		 

                if ((chr == chrUp && inIndex > 0 && pName[inIndex - 1] != chrUpPrev) ||
                                (inIndex > 2 && pName[inIndex] == chrUp &&
                                 pName[inIndex - 1] == chrUpPrev &&
                                 pName[inIndex - 2] == chrUpPrev2))

                        g_string_append_c (buffer, '_');

                g_string_append_c (buffer, g_ascii_tolower (chr));
				inIndex++;
        }

        g_string_append (buffer, "_get_type");
        pSymbol = g_string_free (g_steal_pointer (&buffer), FALSE);
        UI_UTILITY_DEBUG("UI_UTILITY: symbol = %s\n", pSymbol);
        if (g_module_symbol (pModule, pSymbol, (gpointer)&function))
        {
                gtype = function ();
        }

        g_free (pSymbol);

        return gtype;
}

/**
 * thornbury_ui_utility_get_gtype_from_name
 * @pName: GObject name
 *
 * Returns: GType
 *
 * This function is used to get the GType of a GObject. 
 */
GType thornbury_ui_utility_get_gtype_from_name(const gchar *pName)
{
	GType gtype = G_TYPE_INVALID;
	gtype = get_type_from_name(pName);
	return gtype;
}
