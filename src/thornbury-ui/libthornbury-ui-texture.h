/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __THORNBURY_UI_TEXTURE_H__
#define __THORNBURY_UI_TEXTURE_H__

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <clutter/clutter.h>
#include<glib.h>
#include <gdk-pixbuf/gdk-pixdata.h>

G_BEGIN_DECLS

ClutterActor *thornbury_ui_texture_create_new (const gchar *path,
                                               gint width,
                                               gint height,
                                               gboolean preserve_aspect_ratio,
                                               gboolean async);

void thornbury_ui_texture_set_from_file (ClutterActor *image_actor,
                                         const gchar *path,
                                         gint width,
                                         gint height,
                                         gboolean preserve_aspect_ratio,
                                         gboolean async);

ClutterActor *thornbury_ui_texture_create_new_for_resource (const gchar *path,
							    gfloat width,
							    gfloat height,
							    GError **error);

void thornbury_ui_texture_set_from_resource (ClutterActor *image_actor, 
					     const gchar *path,
					     gfloat width,
					     gfloat height,
					     GError **error);

G_END_DECLS

#endif
