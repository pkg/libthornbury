/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*style.c */


#include "libthornbury-widgetstyler.h"
#include <clutter/clutter.h>

#define STYLE_DEBUG_PRINT(...)         //g_print(__VA_ARGS__)

#define _STYLE_MAX_LAYER	10

//static GHashTable *pMainHash = NULL;
//static GHashTable *pNoLayerHash = NULL;

static GList *pLayerList = NULL;
static GList *pMainList = NULL;

static void
widgetstyle_value_free (gpointer data)
{
  GValue *value = data;
  g_value_unset (value);
  g_free (value);
}

static void
v_style_iter_property_table(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	if(NULL != pKey && NULL != pValue && NULL != pUserData)
	{
		GObject *pObj = pUserData;
		guint uinPropType = G_VALUE_TYPE(pValue);
		gchar *pPropertyName = g_strdup(pKey);

		switch(uinPropType)
		{
			case G_TYPE_BOOLEAN:
				{
					gboolean bVal = g_value_get_boolean(pValue);
					g_object_set(pObj, pPropertyName, bVal, NULL);
					break;
				}
			case G_TYPE_INT:
				{
					gint inVal = g_value_get_int(pValue);
					g_object_set(pObj, pPropertyName, inVal, NULL);
					break;
				}
			case G_TYPE_INT64:
				{
					gint64 inVal = g_value_get_int64(pValue);
					g_object_set(pObj, pPropertyName, inVal, NULL);
					break;
				}

			case G_TYPE_UINT:
				{
					guint uinVal = g_value_get_uint(pValue);
					g_object_set(pObj, pPropertyName, uinVal, NULL);

					break;
				}
			case G_TYPE_UINT64:
				{
					guint64 ulnVal = g_value_get_uint64(pValue);
					g_object_set(pObj, pPropertyName, ulnVal, NULL);

					break;
				}
			case G_TYPE_LONG:
				{
					glong lnVal = g_value_get_long(pValue);
					g_object_set(pObj, pPropertyName, lnVal, NULL);

					break;
				}
			case G_TYPE_ULONG:
				{
					gulong ulnVal = g_value_get_ulong(pValue);
					g_object_set(pObj, pPropertyName, ulnVal, NULL);

					break;
				}
			case G_TYPE_FLOAT:
				{
					gfloat fltVal = g_value_get_float(pValue);
					g_object_set(pObj, pPropertyName, fltVal, NULL);

					break;
				}
			case G_TYPE_STRING:
				{
					const gchar* value = g_value_get_string (pValue);
					if(g_strcmp0(pKey, "color") == 0)
					{
						ClutterColor color = {0};
						clutter_color_from_string (&color, value);
						g_object_set(pObj, pPropertyName, &color, NULL);
					}
					else if(g_strcmp0(pKey, "filename") == 0)
					{
						/* style has to look into standard path to get file */
						gchar *filepath = g_strdup_printf (DATADIR "/%s", value);
						g_object_set(pObj, pPropertyName, filepath, NULL);
						g_free (filepath);
					}
					else
						g_object_set (pObj, pPropertyName, value, NULL);
					break;
				}
			case G_TYPE_DOUBLE:
				{
					gdouble dVal = g_value_get_double(pValue);
					g_object_set(pObj, pPropertyName, dVal, NULL);

					break;
				}
			case G_TYPE_POINTER:
				{
					gpointer pVal = g_value_get_pointer(pValue);
					if(g_strcmp0(pKey, "color") == 0)
					{
						ClutterColor *color;
						color = (ClutterColor *)pVal;
						if(color)
						{
							g_object_set(pObj, pPropertyName, color, NULL);
						}
					}
					break;
				}
			case G_TYPE_ENUM:
				{

					break;
				}
			case G_TYPE_INVALID:
			default:
				break;
		}
	}
}

static gint
v_find_layer_list(gpointer pData, gpointer pUserData)
{
        guint uinLayer = *(guint*)pUserData;
        guint uinListData = *(guint*)pData;

        if(uinLayer == uinListData)
        {
                STYLE_DEBUG_PRINT("data = %d, layer = %d\n", uinListData, uinLayer);
                return 0;
        }
        return 1;
}

static gint
v_comp_layer_list(gpointer pData, gpointer pUserData)
{
        guint uinLayer = *(guint*)pUserData;
        guint uinListData = *(guint*)pData;

        if(uinLayer < uinListData)
        {
                STYLE_DEBUG_PRINT("data = %d, layer = %d\n", uinListData, uinLayer);
                return 0;
        }
        return 1;
}

static void
v_style_set_value(gchar *pKey, ThornburyWidgetParserData *pThornburyWidgetParserData, GHashTable *pNoLayerHash)
{
	if(pThornburyWidgetParserData->uinType != G_TYPE_INVALID)
	{
		GValue *pData = NULL;
		GHashTable *pHashNew = NULL;

		if(pThornburyWidgetParserData->uinType == G_TYPE_OBJECT)
		{
			ThornburyWidgetObjectData *pObjData =  pThornburyWidgetParserData->pObjData;
			if(pObjData && pObjData->uinObjectType != G_TYPE_INVALID)
			{
				GType uinType = pObjData->uinObjectType;
				GObject *pPropertyObj = g_object_new(uinType, NULL);
				if(pPropertyObj)
				{
					/* set the properties for the object */
					if(pObjData->pPropertyHash)
					{
						g_hash_table_foreach(pObjData->pPropertyHash, v_style_iter_property_table, pPropertyObj);
						pData = g_new0 (GValue, 1);
						g_value_init (pData, pObjData->uinObjectType);
						g_value_set_object (pData, pPropertyObj);
					}

					/* maintain the layer list to store the objects based on their layering */
					if(pLayerList == NULL)
                                        {
						pLayerList = g_list_append(pLayerList, &(pObjData->uinLayer));
						/* for each layer, one hash is maintained */
						pHashNew = g_hash_table_new_full (g_str_hash, g_direct_equal,
						                                  g_free, (GDestroyNotify) widgetstyle_value_free);
						g_hash_table_insert(pHashNew, pKey, pData);
                                                pMainList = g_list_append(pMainList,  pHashNew);
					}
					else
					{
						GList *pList = NULL;
						GHashTable *pHash = NULL;
						/* if layer is already available, get the relative hash and update the same */
                                                pList = g_list_find_custom(pLayerList, &(pObjData->uinLayer), (GCompareFunc)v_find_layer_list);
                                                if(pList)
                                                {
							guint index = g_list_position(pLayerList, pList);
							pHash = g_list_nth_data(pMainList, index);
							g_hash_table_insert(pHash, pKey, pData);
						}
						/* if new layer, insert it with sorting and  create a new hash */
						else
						{
							/* check if the layer is smaller than the existing layers in the list */
							pList = g_list_find_custom(pLayerList, &(pObjData->uinLayer), (GCompareFunc)v_comp_layer_list);
							if(pList)
							{
								/* if layer is smaller, insert new layer before that list element */
								guint uinIndex = g_list_position(pLayerList, pList );
								STYLE_DEBUG_PRINT("index to insert the layer = %d\n", uinIndex);
								pLayerList = g_list_insert(pLayerList, &(pObjData->uinLayer), g_list_position(pLayerList,pList ));
								/*  new hash to store key/value for the new layer */
								pHashNew = g_hash_table_new_full (g_str_hash, g_direct_equal,
						                                                  g_free, (GDestroyNotify) widgetstyle_value_free);
								g_hash_table_insert(pHashNew, pKey, pData);
								/* update the main list with new hash at given index */
								pMainList = g_list_insert(pMainList,  pHashNew, uinIndex);
							}
							else
							{
								/* otherwise append at last */
								STYLE_DEBUG_PRINT("append layer at last\n"  );
                                                        	pLayerList = g_list_append(pLayerList, &(pObjData->uinLayer));
								pHashNew = g_hash_table_new_full (g_str_hash, g_direct_equal,
						                                                  g_free, (GDestroyNotify) widgetstyle_value_free);
	                                                        g_hash_table_insert(pHashNew, pKey, pData);
								pMainList = g_list_append(pMainList,  pHashNew);
                                                	}
						}
					}
				}
			}

		}
		else
		{
			GType type = G_VALUE_TYPE(pThornburyWidgetParserData->pValue);
			STYLE_DEBUG_PRINT("STYLE : property type = %s\n", g_type_name(type));

			pData = g_new0 (GValue, 1);
			g_value_init (pData, type);
			g_value_copy (pThornburyWidgetParserData->pValue, pData);

			/* for the styles which do not need any layering */
			//if(NULL == pNoLayerHash)
			//{
			//	pNoLayerHash = g_hash_table_new (g_str_hash, g_direct_equal);
			//}
                        g_hash_table_insert(pNoLayerHash, pKey, pData);
		}

	}
}

static void
v_style_iter_hash(gpointer pKey, gpointer pValue, gpointer pUserData)
{
	gchar *pStyleKey = g_strdup(pKey);
	ThornburyWidgetParserData *pThornburyWidgetParserData = pValue;

	STYLE_DEBUG_PRINT("STYLE : key = %s\n", pStyleKey);

	if(pThornburyWidgetParserData)
	{
		v_style_set_value(pStyleKey,  pThornburyWidgetParserData, pUserData);
	}

}

static GHashTable *
style_set_from_parser_hash (GHashTable *pParserHash)
{
	GHashTable *pNoLayerHash = NULL;
	GHashTable *pMainHash = NULL;

	/* pNoLayerHash contains string/GValue pairs. */
	pNoLayerHash = g_hash_table_new_full (g_str_hash, g_direct_equal,
	                                      g_free, (GDestroyNotify) widgetstyle_value_free);

	if(NULL != pParserHash)
	{
		g_hash_table_foreach(pParserHash, v_style_iter_hash, pNoLayerHash);
	}

	/* create the main hash which maintains one hash table for each layer */
	if(NULL != pMainList || NULL != pNoLayerHash)
	{
		GList *list = g_list_first(pMainList);
		/* main hash table : key = layer i.e. 0, 1, 2, .., NoLayer
		 * value = hash table -> key = style name i.e. background, font-family, ...
		 *			 value =  GValue for the style
		*/
		pMainHash = g_hash_table_new_full (g_str_hash, g_direct_equal,
		                                   g_free, (GDestroyNotify) g_hash_table_unref);
		while(list && list->data)
		{
			GHashTable *hash = list->data;
			g_hash_table_insert(pMainHash, g_strdup_printf("%d", g_list_position(pMainList, list)), hash);
			list = g_list_next(list);
		}

		/* hash table maintained for style properties which do not need layer info */
		if(NULL != pNoLayerHash)
			g_hash_table_insert(pMainHash, g_strdup("NoLayer"), pNoLayerHash);
	}

	g_list_free_full (pMainList, g_free);

	return pMainHash;
}

/**
 * thornbury_style_set:
 * @style_file: file to be parsed in JSON format
 * Returns: Hash Table where,
 *	     key -> layer (hierarchy of objects to be shown),
 *		   "NoLayer" in case of layering not provided
 *	     value -> HashTable of objects for each layer where,
 *			key -> style name
 *			value -> style object as GValue
 *
 * Returns: (transfer full): All the properties mentioned in json file returned in Hash Table format based on layering.
 */
GHashTable *
thornbury_style_set (const gchar *style_file)
{
	GHashTable *pParserHash = NULL;
	GHashTable *pMainHash = NULL;

	g_return_val_if_fail (style_file != NULL, NULL);

	pParserHash = thornbury_parser_parse_style_file (style_file);
	pMainHash = style_set_from_parser_hash (pParserHash);
	thornbury_parser_free_hash (pParserHash);

	return pMainHash;
}

/**
 * thornbury_style_free:
 * @style_hash: Hash table to be freed
 *
 *  Frees the style hash table.
 */
void
thornbury_style_free (GHashTable *style_hash)
{
  g_return_if_fail (style_hash != NULL);

  g_hash_table_destroy (style_hash);
}

/**
 * thornbury_style_set_from_resource:
 * @path: path to the style JSON file, in the #GResource namespace
 *
 * Version of thornbury_style_set() which loads from a file stored in the global
 * #GResource namespace.
 *
 * Any errors in finding or parsing the file are fatal; it is a programmer error
 * to provide an invalid or missing file to this function.
 *
 * Returns: (transfer full): layered hash table containing properties
 * Since: UNRELEASED
 */
GHashTable *
thornbury_style_set_from_resource (const gchar *path)
{
  GHashTable *parser_hash = NULL;
  GHashTable *main_hash = NULL;
  g_autoptr (GError) error = NULL;

  g_return_val_if_fail (path != NULL, NULL);

  parser_hash = thornbury_parser_parse_style_resource (path);
  main_hash = style_set_from_parser_hash (parser_hash);
  thornbury_parser_free_hash (parser_hash);

  return main_hash;
}
