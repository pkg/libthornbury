/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/* Copyright (c) 2015-2016 Collabora */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <glib.h>
#include <gio/gio.h>

#include "libthornbury-widgetparser.h"

/* Generated file defines @property_json_instances. */
#include "property-parser-vectors.h"

/* Generated file defines @style_json_instances. */
#include "style-parser-vectors.h"

static void
test_parser (guint      i,
             gboolean   is_property)
{
  const char *vector_json;
  size_t vector_size;
  unsigned int vector_is_valid;
  GFile *test_file = NULL;
  GFileIOStream *io_stream = NULL;
  GOutputStream *output_stream;
  GHashTable *result = NULL;
  gchar *test_file_path = NULL;
  GError *error = NULL;

  if (is_property)
    {
      vector_json = property_json_instances[i].json;
      vector_size = property_json_instances[i].size;
      vector_is_valid = property_json_instances[i].is_valid;
    }
  else
    {
      vector_json = style_json_instances[i].json;
      vector_size = style_json_instances[i].size;
      vector_is_valid = style_json_instances[i].is_valid;
    }

  /* Run the test in a subprocess to handle parser crashes; but only do so if
   * -m=slow is specified, as this really slows things down. */
  if (g_test_slow () && !g_test_subprocess ())
    {
      g_test_trap_subprocess (NULL, 0, 0);

      /* Note that the parser code does not print any warnings or error messages
       * on parse failures (which is good). */
      g_test_trap_assert_stderr ("");

      g_test_trap_assert_passed ();

      return;
    }

  /* Dump the test vector out to a temporary file for the parser. */
  test_file = g_file_new_tmp ("widget-parser-XXXXXX.json",
                              &io_stream, &error);
  g_assert_no_error (error);

  output_stream = g_io_stream_get_output_stream (G_IO_STREAM (io_stream));
  g_output_stream_write_all (output_stream, vector_json,
                             vector_size, NULL, NULL, &error);
  g_assert_no_error (error);

  g_io_stream_close (G_IO_STREAM (io_stream), NULL, &error);
  g_assert_no_error (error);

  test_file_path = g_file_get_path (test_file);

  if (is_property)
    {
      /* Try parsing as a prop file. */
      result = thornbury_parser_parse_prop_file (test_file_path);
    }
  else
    {
      /* Try parsing as a style file. */
      result = thornbury_parser_parse_style_file (test_file_path);
    }

  /* Note that both parser functions will not reliably return NULL on failure —
   * they return NULL on parse failures at the top level of the instance, but
   * not at lower levels. */
  g_clear_pointer (&result, thornbury_parser_free_hash);

  /* Tidy up. */
  g_file_delete (test_file, NULL, &error);
  g_assert_no_error (error);

  g_free (test_file_path);
  g_clear_object (&io_stream);
  g_clear_object (&test_file);
}

static void
test_property_parser (gconstpointer   user_data)
{
  guint i = GPOINTER_TO_UINT (user_data);
  test_parser (i, TRUE);
}

static void
test_style_parser (gconstpointer   user_data)
{
  guint i = GPOINTER_TO_UINT (user_data);
  test_parser (i, FALSE);
}

int
main (int    argc,
      char **argv)
{
  guint i;

  g_test_init (&argc, &argv, NULL);

  for (i = 0; i < G_N_ELEMENTS (property_json_instances); i++)
    {
      gchar *test_name = NULL;

      test_name = g_strdup_printf ("/libthornbury/widget-parser/property-parser/%u", i);
      g_test_add_data_func (test_name, GUINT_TO_POINTER (i),
                            test_property_parser);
      g_free (test_name);
    }

  for (i = 0; i < G_N_ELEMENTS (style_json_instances); i++)
    {
      gchar *test_name = NULL;

      test_name = g_strdup_printf ("/libthornbury/widget-parser/style-parser/%u", i);
      g_test_add_data_func (test_name, GUINT_TO_POINTER (i),
                            test_style_parser);
      g_free (test_name);
    }

  return g_test_run ();
}
