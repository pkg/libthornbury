/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:thornbury-widgetparser/libthornbury-widgetparser.h
 * @title: ThornburyWidgetParser
 * @short_description: Thornbury Widget Parser to parse the widget property JSON files.
 *
 * Each widget would have a default configuration file associated with it
 * describing its properties.
 *
 * Application developers can choose the required properties for the widget in a
 * particular view.During  the widget creation process, its properties are
 * extracted from the widget configuration file and fed to the
 * #ThornburyItemFactory.
 *
 * #ThornburyItemFactory would then set the corresponding properties on the
 * widget.
 *
 * This widget configuration file is in JSON format (Java Script Object
 * Notation). 
 *
 * Using ThornburyWidgetParserData class a special parser library will
 * be available in the Thornbury SDK to parse the entries  in this file and
 * feed the properties to the [](ThornburyItemFactory) before the widget
 * generation process.
 *
 * Sample Code:
 *
 * ```C
 * pPropHash = thornbury_parser_parse_prop_file(DATADIR"/test_widget.json");
 * if(pPropHash)
 * {
 *   g_print("TEST_PARSER : parsing style properties .............\n");
 * 	 g_hash_table_foreach(pPropHash, v_test_parser_iter_prop_table, NULL);
 * }
 *
 * void v_test_parser_iter_prop_table(gpointer pKey, gpointer pValue, gpointer pUserData)
 * {
 * 	 g_print("TEST_PARSER : key = %s \n",(gchar*) pKey);
 *   ThornburyWidgetParserData *pThornburyWidgetParserData = pValue;
 *   if(pThornburyWidgetParserData)
 *	 {
 *		 g_print("TEST_PARSER : type = %s\n", g_type_name(pThornburyWidgetParserData->uinType));
 *  	 g_print("TEST_PARSER : prop name = %s\n", pThornburyWidgetParserData->propName);
 *		 if(G_IS_VALUE (pThornburyWidgetParserData->pValue))
 *		 {
 *			 g_print("TEST_PARSER : getting value .....\n");
 *			 if(G_VALUE_HOLDS_STRING (pValue))
 *			 {
 *			 	 g_print("TEST_PARSER : value = %s\n", g_value_get_string(pThornburyWidgetParserData->pValue));
 *			 }
 *			 else if(G_VALUE_HOLDS_INT64 (pThornburyWidgetParserData->pValue))
 *				 g_print("TEST_PARSER : value = %d\n", (gint)g_value_get_int64(pThornburyWidgetParserData->pValue));
 *		 }
 *	 }
 * }
 *
 * ```
 *
 * Sample JSON:
 * 
 * ```json 
 * {
 *   "3":{
 *     "roller-type":{
 *     "gint":10
 *     }
 *   },
 *   "1":{
 *     "width":{
 *     "gfloat":392.0
 *     }
 *   },
 *   "2":{
 *     "height":{
 *     "gfloat":480.0
 *     }
 *   },
 *   "4":{
 *     "roll-over":{
 *     "gboolean":true
 *     }
 *   },
 *   "5":{
 *     "reactive":{
 *     "gboolean":true
 *     }
 *   },
 *   "6":{
 *     "arrow":{
 *     "gboolean":true
 *     }
 *   }
 * }
 *
 * ```
 *
 * WidgetParser is available since thornbury-base 1.0
 */

#ifndef _PARSER_H
#define _PARSER_H

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <glib-object.h>
#include <string.h>

G_BEGIN_DECLS


typedef struct _ThornburyWidgetParserData ThornburyWidgetParserData;
typedef struct _ThornburyWidgetObjectData ThornburyWidgetObjectData;

/**
 * ThornburyWidgetObjectData:
 * @uinObjectType: GObject Type
 * @uinLayer: Layer
 * @pPropertyHash: GObject property name with value.
 *
 * Struct for the gobject type.
 */
struct _ThornburyWidgetObjectData
{
 GType uinObjectType;
 guint uinLayer;
 GHashTable *pPropertyHash;
};


/**
 * ThornburyWidgetParserData:
 * @uinType: GType for the key value
 * 
 */
struct _ThornburyWidgetParserData
{
 GType uinType;
 union
 {
 ThornburyWidgetObjectData *pObjData;
 GValue *pValue;
 };
 gchar *propName;
 guint uinIndex;
};


//GVariant *p_PARSER_parse_widget(gchar *pFile);

GHashTable* thornbury_parser_parse_prop_file (const gchar *pFile);
GHashTable* thornbury_parser_parse_style_file (const gchar *pFile);
void thornbury_parser_free_hash(GHashTable *pHash);

GHashTable *thornbury_parser_parse_prop_resource (const gchar *path);
GHashTable *thornbury_parser_parse_style_resource (const gchar *path);

G_END_DECLS

#endif /* _PARSER_H */
