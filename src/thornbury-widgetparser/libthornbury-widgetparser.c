/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * parser.c
 *
 *
 * parser.c */



#include "libthornbury-widgetparser.h"

#include <glib-object.h>
#include <string.h>
#include <json-glib/json-glib.h>
#include <clutter/clutter.h>

#define PARSER_DEBUG_PRINT(...)		//g_print(__VA_ARGS__)

static void
v_parser_free_property_hash_entry(gpointer pKey, gpointer pValue, gpointer puserData)
{
	if(NULL != pValue)
	{
		PARSER_DEBUG_PRINT("PARSER : free property value\n");
		g_free(pValue);
		pValue = NULL;
	}
	if(NULL != pKey)
	{
		PARSER_DEBUG_PRINT("PARSER : free property key\n");
		g_free(pKey);
		pKey = NULL;
	}

}

static void
v_parser_free_value(ThornburyWidgetParserData *pThornburyWidgetParserData)
{
	if(NULL == pThornburyWidgetParserData->pObjData)
		return;
	
	if(pThornburyWidgetParserData->uinType == G_TYPE_OBJECT && NULL != pThornburyWidgetParserData->pObjData->pPropertyHash)
	{
		PARSER_DEBUG_PRINT("PARSER : free sub hash\n");
		g_hash_table_foreach(pThornburyWidgetParserData->pObjData->pPropertyHash, v_parser_free_property_hash_entry, NULL);
		g_hash_table_destroy(pThornburyWidgetParserData->pObjData->pPropertyHash);
		pThornburyWidgetParserData->pObjData->pPropertyHash = NULL;
		g_free(pThornburyWidgetParserData->pObjData);
		pThornburyWidgetParserData->pObjData = NULL;
	}

	else if(NULL != pThornburyWidgetParserData->pValue)
	{
		PARSER_DEBUG_PRINT("PARSER : free value\n");
		g_free(pThornburyWidgetParserData->pValue);
		pThornburyWidgetParserData->pValue = NULL;

	}
}

static void
v_parser_free_style_hash_entry(gpointer pKey, gpointer pValue, gpointer puserData)
{
	if(NULL != pValue)
	{
		ThornburyWidgetParserData *pThornburyWidgetParserData = pValue;
		v_parser_free_value(pThornburyWidgetParserData);

		if(NULL != pThornburyWidgetParserData->propName)
		{
			PARSER_DEBUG_PRINT("PARSER : free prop name\n");
			g_free(pThornburyWidgetParserData->propName);
			pThornburyWidgetParserData->propName = NULL;
		}
		if(pThornburyWidgetParserData)
		{
			g_free(pThornburyWidgetParserData);
			pThornburyWidgetParserData = NULL;
		}
	}
	if(NULL != pKey)
	{
		PARSER_DEBUG_PRINT("PARSER : key\n");
		g_free(pKey);
		pKey = NULL;
	}
}

static void
v_parser_iter_property_node(JsonObject *pJsonObject,
                            const gchar *pMemberName,
                            JsonNode *pMemberNode,
                            gpointer pUserData)
{
        ThornburyWidgetObjectData *pData = pUserData;

	if(g_strcmp0(pMemberName, "color") == 0)
	{
		PARSER_DEBUG_PRINT("PARSER : value type = %s\n", g_type_name(json_node_get_value_type(pMemberNode)));
                PARSER_DEBUG_PRINT("PARSER : last property name = %s\n", pMemberName);

		if(JSON_NODE_HOLDS_ARRAY(pMemberNode))
		{
			JsonArray *pArray = json_node_get_array (pMemberNode);
			ClutterColor color = { 0, };
			if (json_array_get_length (pArray) == 3 ||
					json_array_get_length (pArray) == 4)
			{
				GValue *pVal;
				ClutterColor *pColor;

				color.red   = CLAMP (json_array_get_int_element (pArray, 0), 0, 255);
				color.green = CLAMP (json_array_get_int_element (pArray, 1), 0, 255);
				color.blue  = CLAMP (json_array_get_int_element (pArray, 2), 0, 255);

				if (json_array_get_length (pArray) == 4)
					color.alpha = CLAMP (json_array_get_int_element (pArray, 3), 0, 255);
				else
					color.alpha = 255;

	                        pVal = g_new0(GValue, 1);
				g_value_init(pVal, CLUTTER_TYPE_COLOR);
				pColor = clutter_color_new (color.red, color.green, color.blue, color.alpha);
				clutter_value_set_color (pVal, pColor);
				g_hash_table_insert(pData->pPropertyHash, (gpointer)g_strdup(pMemberName), pVal);
			}

		}
		else if(json_node_get_value_type(pMemberNode) == G_TYPE_STRING)
		{
			GValue *pVal;
			GValue pGvalue = G_VALUE_INIT;
			json_node_get_value(pMemberNode, &pGvalue);

			pVal = g_new0(GValue, 1);
			memcpy(pVal , &pGvalue, sizeof(pGvalue));
			g_hash_table_insert(pData->pPropertyHash, (gpointer)g_strdup(pMemberName), pVal);
		}
		else
		{
			;
		}

	}

        if(JSON_NODE_HOLDS_VALUE(pMemberNode))
	{
		GValue *pVal;
		GValue pGvalue = G_VALUE_INIT;
		json_node_get_value(pMemberNode, &pGvalue);

		pVal = g_new0(GValue, 1);
		memcpy(pVal , &pGvalue, sizeof(pGvalue));
		g_hash_table_insert(pData->pPropertyHash, (gpointer)g_strdup(pMemberName), pVal);
	}

}

static void
v_parser_iter_last_node (JsonObject *pJsonObject,
                            const gchar *pMemberName,
                            JsonNode *pMemberNode,
                            gpointer pUserData)
{

        JsonObject *pMainObject = NULL;
	ThornburyWidgetObjectData *pData = pUserData;

        if(JSON_NODE_HOLDS_OBJECT(pMemberNode))
        {
                GList *pList;
		GType type = g_type_from_name(pMemberName);
		if(type)
		{
                	pData->uinObjectType = type;
			PARSER_DEBUG_PRINT("PARSER : obj type = %s\n", g_type_name(pData->uinObjectType));
		}
		pMainObject = json_node_get_object(pMemberNode);
                pList = json_object_get_members (pMainObject);
                if(pList)
                {
                        guint uinLen = g_list_length(pList);
                        if(uinLen)
                        {
                        	json_object_foreach_member(pMainObject,
                                                           v_parser_iter_property_node,
                                                           pData);

                        }
                }

        }
        else if(JSON_NODE_HOLDS_VALUE(pMemberNode))
	{
		PARSER_DEBUG_PRINT("PARSER : value type = %s\n", g_type_name(json_node_get_value_type(pMemberNode)));
		PARSER_DEBUG_PRINT("PARSER : object layer = %s\n", pMemberName);

		pData->uinLayer = json_node_get_int(pMemberNode);
	}
	else
	{
		/* do nothing */
		;
	}
}


static void v_parser_iter_style_sub_node (JsonObject *pJsonObject,
                            const gchar *pMemberName,
                            JsonNode *pMemberNode,
                            gpointer pUserData)
{
        JsonObject *pMainObject = NULL;
	ThornburyWidgetParserData *pData = pUserData;

        if(JSON_NODE_TYPE (pMemberNode) == JSON_NODE_OBJECT)
        {
		GList *pList;
		GType type = g_type_from_name(pMemberName);
                if(type)
                {
                        pData->uinType = type;
                        PARSER_DEBUG_PRINT("PARSER : obj type = %s\n", g_type_name(pData->uinType));
                }

                pMainObject = json_node_get_object(pMemberNode);
                pList = json_object_get_members (pMainObject);
                if(pList && (g_list_length(pList) > 0) )
		{
				pData->pObjData = g_new0(ThornburyWidgetObjectData, 1);
				pData->pObjData->pPropertyHash = g_hash_table_new (g_str_hash, g_direct_equal);

				json_object_foreach_member(pMainObject,
						v_parser_iter_last_node,
						pData->pObjData);

                }
        }
        else if(JSON_NODE_TYPE (pMemberNode) == JSON_NODE_VALUE)
        {
		GValue pGvalue = G_VALUE_INIT;
		GType type = g_type_from_name(pMemberName);
		if(type)
		{
			pData->uinType = type;
			PARSER_DEBUG_PRINT("PARSER : sub value type = %s\n", g_type_name(pData->uinType));
		}

		json_node_get_value(pMemberNode, &pGvalue);
		pData->pValue = g_new0(GValue, 1);
		memcpy(pData->pValue , &pGvalue, sizeof(pGvalue));
# if 0
		if(G_VALUE_HOLDS_STRING (pData->pValue))
		{
			PARSER_DEBUG_PRINT ("PARSER : converted sub gvalue %s\n", g_value_get_string (pData->pValue));
		}
# endif
	}


}

static void v_parser_iter_style_node  (JsonObject *pJsonObject,
                               const gchar *pMemberName,
                               JsonNode *pMemberNode,
                               gpointer pUserData)
{
	GHashTable *pPropertyHash = pUserData;
	ThornburyWidgetParserData *pData = g_new0(ThornburyWidgetParserData, 1);

        JsonObject *pMainObject = NULL;
        if(JSON_NODE_TYPE (pMemberNode) == JSON_NODE_OBJECT)
        {
                GList *pMainList;

                pMainObject = json_node_get_object(pMemberNode);
                pMainList = json_object_get_members (pMainObject);
                if(pMainList)
                {
                        gint inMainLen = 0;
                        inMainLen = g_list_length(pMainList);
                        if(inMainLen)
                        {
                                PARSER_DEBUG_PRINT("PARSER : style main length = %d\n", inMainLen);
                                json_object_foreach_member(pMainObject,
                                                           v_parser_iter_style_sub_node,
                                                           pData);
                        }
                }
        }

	/* update the hash with key  and value */
	if(NULL != pMemberName)
	{
		g_hash_table_insert(pPropertyHash, (gpointer)g_strdup(pMemberName), pData);
	}

}

static void v_parser_iter_prop_next_sub_node (JsonObject *pJsonObject,
                            const gchar *pMemberName,
                            JsonNode *pMemberNode,
                            gpointer pUserData)
{
        JsonObject *pMainObject = NULL;
        ThornburyWidgetParserData *pData = pUserData;

        if(JSON_NODE_TYPE (pMemberNode) == JSON_NODE_OBJECT)
        {
                GList *pList;
                GType type = g_type_from_name(pMemberName);
                if(type)
                {
                        pData->uinType = type;
                        PARSER_DEBUG_PRINT("PARSER : obj type = %s\n", g_type_name(pData->uinType));
                }

                pMainObject = json_node_get_object(pMemberNode);
                pList = json_object_get_members (pMainObject);
                if(pList && (g_list_length(pList) > 0) )
                {
			pData->pObjData = g_new0(ThornburyWidgetObjectData, 1);
			pData->pObjData->pPropertyHash = g_hash_table_new (g_str_hash, g_direct_equal);

			json_object_foreach_member(pMainObject,
					v_parser_iter_last_node,
					pData->pObjData);
		}
        }
        else if(JSON_NODE_TYPE (pMemberNode) == JSON_NODE_VALUE)
        {
		GValue pGvalue = G_VALUE_INIT;
		GType type = g_type_from_name(pMemberName);
		if(type)
		{
			pData->uinType = type;
			PARSER_DEBUG_PRINT("PARSER : sub value type = %s\n", g_type_name(pData->uinType));
		}

		json_node_get_value(pMemberNode, &pGvalue);
		pData->pValue = g_new0(GValue, 1);
		memcpy(pData->pValue , &pGvalue, sizeof(pGvalue));
# if 0
		if(G_VALUE_HOLDS_STRING (pData->pValue))
		{
			PARSER_DEBUG_PRINT ("PARSER : converted sub gvalue %s\n", g_value_get_string (pData->pValue));
		}
		else if(G_VALUE_HOLDS_INT64 (pData->pValue))
                {
                        PARSER_DEBUG_PRINT ("PARSER : converted sub gvalue %d\n", (gint)g_value_get_int64 (pData->pValue));
                }
# endif
        }
}


static void v_parser_iter_prop_sub_node (JsonObject *pJsonObject,
                            const gchar *pMemberName,
                            JsonNode *pMemberNode,
                            gpointer pUserData)
{
        JsonObject *pMainObject = NULL;
        ThornburyWidgetParserData *pData = pUserData;

        if(JSON_NODE_TYPE (pMemberNode) == JSON_NODE_OBJECT)
        {
		GList *pMainList;
                PARSER_DEBUG_PRINT("PARSER : sub name = %s\n", pMemberName);
		pData->propName = g_strdup(pMemberName);
		pMainObject = json_node_get_object(pMemberNode);
		pMainList = json_object_get_members (pMainObject);
                if(pMainList)
                {
                        gint inMainLen = 0;
                        inMainLen = g_list_length(pMainList);
                        if(inMainLen)
                        {
                                PARSER_DEBUG_PRINT("PARSER : sub length = %d\n", inMainLen);
				json_object_foreach_member(pMainObject,
                                                           v_parser_iter_prop_next_sub_node,
                                                           pData);

			}
		}
        }

}

static void v_parser_iter_prop_node  (JsonObject *pJsonObject,
                               const gchar *pMemberName,
                               JsonNode *pMemberNode,
                               gpointer pUserData)
{
        GHashTable *pPropertyHash = pUserData;
        ThornburyWidgetParserData *pData = g_new0(ThornburyWidgetParserData, 1);

        JsonObject *pMainObject = NULL;
        if(JSON_NODE_TYPE (pMemberNode) == JSON_NODE_OBJECT)
        {
                GList *pMainList;
                pMainObject = json_node_get_object(pMemberNode);
                pMainList = json_object_get_members (pMainObject);
                if(pMainList)
                {
                        gint inMainLen = 0;
                        inMainLen = g_list_length(pMainList);
                        if(inMainLen)
                        {
                                json_object_foreach_member(pMainObject,
                                                           v_parser_iter_prop_sub_node,
                                                           pData);
                        }
                }
        }

        /* update the hash with key  and value */
        if(NULL != pMemberName)
        {
                g_hash_table_insert(pPropertyHash, (gpointer)g_strdup(pMemberName), pData);
        }

}

#if 0
/* TO DO: Review if these functions are required */

static void
v_parser_g_value_to_g_variant(GVariantBuilder *gvb, const gchar *pMemberName, GValue *pValue)
{
	if(NULL == gvb || NULL == pValue)
		return;

	GType type;

	type = G_VALUE_TYPE (pValue);
	PARSER_DEBUG_PRINT("PARSER : type = %s\n", g_type_name(type));

	switch(type)
	{
		case G_TYPE_BOOLEAN:
                {
                        PARSER_DEBUG_PRINT("PARSER : value = %d\n", g_value_get_boolean (pValue));
                        g_variant_builder_add_parsed (gvb, "{%s, <%b>}", pMemberName, g_value_get_boolean (pValue));
			break;

                }
                case G_TYPE_STRING:
                {
                        PARSER_DEBUG_PRINT("PARSER : value = %s\n", g_value_get_string (pValue));
                        g_variant_builder_add_parsed (gvb, "{%s, <%s>}", pMemberName, g_value_get_string (pValue));
			break;
                }
                case G_TYPE_INT64:
                {
                        PARSER_DEBUG_PRINT("PARSER : value = %lld\n", g_value_get_int64(pValue));
                        g_variant_builder_add_parsed (gvb, "{%s, <%i>}", pMemberName, g_value_get_int64 (pValue));
			break;
                }
                case G_TYPE_DOUBLE:
                {
                        PARSER_DEBUG_PRINT("PARSER : value = %f\n", g_value_get_double(pValue));
                        g_variant_builder_add_parsed (gvb, "{%s, <%d>}", pMemberName, g_value_get_double (pValue));
			break;
                }

                default:
			PARSER_DEBUG_PRINT("PARSER : %s type not handled\n", g_type_name(type));
	}
}

static void
v_parser_iter_simple_node  (JsonObject *pJsonObject,
                               const gchar *pMemberName,
                               JsonNode *pMemberNode,
                               gpointer pUserData)
{
	PARSER_DEBUG_PRINT("PARSER : property name = %s\n", pMemberName);
	GVariantBuilder *gvb = pUserData;

	if(JSON_NODE_HOLDS_VALUE (pMemberNode))
	{
		GValue value = G_VALUE_INIT;
		json_node_get_value(pMemberNode, &value);
		v_parser_g_value_to_g_variant(gvb, pMemberName, &value);
	}
}

static GVariant*
p_parser_parse_widget_file(gchar *pFile)
{
        JsonNode *pRootNode = NULL;
        JsonObject *pMainObject = NULL;
        gboolean bRet = FALSE;
        GError *PError = NULL;
	GVariant *pParserHash = NULL;
	GVariantBuilder *gvb = NULL;

        JsonParser *pJsonParser = json_parser_new();
        bRet = json_parser_load_from_file(pJsonParser, pFile, &PError);
        if(FALSE == bRet || NULL != PError)
        {
                g_error("error = %s\n", PError->message);
                return NULL;;
        }

        pRootNode = json_parser_get_root(pJsonParser);

        if(pRootNode)
        {
                pMainObject = json_node_get_object(pRootNode);

                if(pMainObject)
                {
			GList *pList = json_object_get_members(pMainObject);
                        PARSER_DEBUG_PRINT("PARSER : main value len = %d\n", g_list_length(pList));


                        if(pList == NULL)
                                return NULL;

                       gvb = g_variant_builder_new (G_VARIANT_TYPE ("a{s*}"));

                       json_object_foreach_member(pMainObject,
                                                  v_parser_iter_simple_node,
                                                  gvb);
                }

	}

	pParserHash = g_variant_builder_end(gvb);
	g_variant_builder_unref(gvb);
	return pParserHash;
}
#endif

/*********************************************************************************************/
/* EXPOSED APIs */
/*********************************************************************************************/

/**
 * thornbury_parser_free_hash:
 * @pHash: GhashTable containing parsed data
 *
 * This function frees the hash containing the parsed data.
 */

void thornbury_parser_free_hash(GHashTable *pHash)
{
	if(NULL != pHash)
	{
		g_hash_table_foreach(pHash, v_parser_free_style_hash_entry, NULL);
		g_hash_table_destroy(pHash);
		pHash = NULL;
	}
}

/**
 * thornbury_parser_parse_json_file:
 * @json_parser: a JSON parser with the file loaded
 * @func: the function to be called on each member.
 * @err: a return location for a GError.
 * 
 * Takes a parsed JSON file and converts into #GHashTable by a given
 * #JsonObjectForeach function.
 * 
 * Returns: (transfer full) (nullable): A #GHashTable which contains parsed data.
 */
static GHashTable *
thornbury_parser_parse_json_file (JsonParser *json_parser,
                                  JsonObjectForeach func,
                                  GError **err)
{
  GHashTable *parsed_table = NULL;
  JsonNode *root_node = NULL;
  JsonObject *main_object = NULL;

  root_node = json_parser_get_root (json_parser);

  if (!JSON_NODE_HOLDS_OBJECT (root_node))
    {
      /* FIXME: Need common error domain for thornbury, T2005 */
      g_set_error (err,
                   g_quark_from_static_string ("thornbury_parser"), 0, 
                   "Root node doesn't hold any JSON Object.");
      return NULL;
    }

  main_object = json_node_get_object (root_node);

  parsed_table = g_hash_table_new (g_str_hash, g_direct_equal);

  json_object_foreach_member (main_object,
                              func,
                              parsed_table);

  return parsed_table;
}

/**
 * thornbury_parser_parse_prop_file:
 * @pFile : File to be parsed containing gobject properties
 *
 * This function is specific to Thornbury Property format file.
 * Parses the json file and returns the parsed data in form of hash table where
 * the key contains the order in which property needs to be set and
 * and value is the structure type of ThornburyWidgetParserData.
 * The ThornburyWidgetParserData structure contains one member as the GType.
 * Another member is property name.
 * There is one member as union which could be:
 * ThornburyWidgetObjectData structure: If the GTYpe is GObject, this structure will have GObject type
 *			 and its properties in hashtable format.The property hash contains
 *		 	 property name nad value. Property name mentioned in the file
 *			 should be the same as supported by the GObject type. Property
 *			 value is in format of GValue.
 * or
 * pGvalue: value for the GType in format of GValue.
 *
 * Returns: Parsed data in the form of hash table.Key,value pair
 */
GHashTable*
thornbury_parser_parse_prop_file (const gchar *pFile)
{
  GError *err = NULL;
  GHashTable *pParserHash = NULL;
  g_autoptr (JsonParser) json_parser = json_parser_new ();

  g_return_val_if_fail (pFile != NULL, NULL);

  if (!json_parser_load_from_file (json_parser, pFile, &err))
    {
      g_printerr ("Failed to load: %s\n", err->message);
      g_clear_error (&err);
      return NULL;
    }

  pParserHash = thornbury_parser_parse_json_file (json_parser,
                                                  v_parser_iter_prop_node,
                                                  &err);

  if (err != NULL)
    {
      /* FIXME: Instead of printing,
       * thornbury_parser_parse_prop_file should handle GError properly.
       * T1996 */
      g_printerr ("Failed to load: %s\n", err->message);
      g_clear_error (&err);
    }

  return pParserHash;
}

/**
 * thornbury_parser_parse_style_file:
 * @pFile : File to be parsed containing style properties
 *
 * This function is specific to Thornbury style format file.
 * Parses the json file and returns the parsed data in form of hash table where
 * the key contains the style name and
 * and value is the structure type of ThornburyWidgetParserData.
 * The ThornburyWidgetParserData structure contains one member as the GType.
 * There is one member as union which could be:
 * ThornburyWidgetObjectData structure: If the GTYpe is GObject, this structure will have GObject type
 *			 and its properties in hashtable format.The property hash contains
 *		 	 property name nad value. Property name mentioned in the file
 *			 should be the same as supported by the GObject type. Property
 *			 value is in format of GValue.
 * or
 * pGvalue: value for the GType in format of GValue.
 *
 * Returns: Parsed data in the form of hash table.Key,value pair.
 */
GHashTable*
thornbury_parser_parse_style_file (const gchar *pFile)
{
  GError *err = NULL;
  GHashTable *pParserHash = NULL;
  g_autoptr (JsonParser) json_parser = json_parser_new ();

  g_return_val_if_fail (pFile != NULL, NULL);

  if (!json_parser_load_from_file (json_parser, pFile, &err))
    {
      g_printerr ("Failed to load: %s\n", err->message);
      g_clear_error (&err);
      return NULL;
    }

  pParserHash = thornbury_parser_parse_json_file (json_parser,
                                                  v_parser_iter_style_node,
                                                  &err);

  if (err != NULL)
    {
      /* FIXME: Instead of printing,
       * thornbury_parser_parse_style_file should handle GError properly.
       * T1996 */
      g_printerr ("Failed to load: %s\n", err->message);
      g_clear_error (&err);
    }

  return pParserHash;
}

# if 0
/*************************************************************
 * p_PARSER_parse_widget:
 * @pFile: File to be parsed
 *
 * Returns: GHashTable*
 *
 * Parses the simple JSON file without nesting
 **************************************************************/
GVariant *
p_PARSER_parse_widget(gchar *pFile)
{
        g_return_val_if_fail (pFile != NULL, FALSE);

        //GHashTable *pParserHash = NULL;
	GVariant *pParserHash = NULL;
        pParserHash = p_parser_parse_widget_file(pFile);
        return pParserHash;

}
# endif

/**
 * thornbury_parser_parse_style_resource:
 * @path: path to the file containing style properties, in the #GResource
 *    namespace
 *
 * Version of thornbury_parser_parse_style_file() which loads from a file
 * stored in the global #GResource namespace.
 *
 * Any errors in finding or parsing the file are fatal; it is a programmer error
 * to provide an invalid or missing file to this function.
 *
 * Returns: (transfer full): parsed data as key–value pairs
 * Since: UNRELEASED
 */
GHashTable *
thornbury_parser_parse_style_resource (const gchar *path)
{
  g_autoptr (JsonParser) parser = NULL;
  g_autoptr (GInputStream) stream = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GHashTable) output = NULL;

  g_return_val_if_fail (path != NULL, NULL);

  parser = json_parser_new ();
  stream = g_resources_open_stream (path, G_RESOURCE_LOOKUP_FLAGS_NONE, &error);

  if (error != NULL)
    {
      g_error ("Error parsing style resource ‘%s’: %s", path, error->message);
      return NULL;
    }

  if (!json_parser_load_from_stream (parser, stream, NULL, &error))
    {
      g_error ("Error parsing style resource ‘%s’: %s", path, error->message);
      return NULL;
    }

  output = thornbury_parser_parse_json_file (parser, v_parser_iter_style_node,
                                             &error);

  if (error != NULL)
    {
      g_error ("Error parsing style resource ‘%s’: %s", path, error->message);
      return NULL;
    }

  return g_steal_pointer (&output);
}

/**
 * thornbury_parser_parse_prop_resource:
 * @path: path to the file containing prop properties, in the #GResource
 *    namespace
 *
 * Version of thornbury_parser_parse_prop_file() which loads from a file
 * stored in the global #GResource namespace.
 *
 * Any errors in finding or parsing the file are fatal; it is a programmer error
 * to provide an invalid or missing file to this function.
 *
 * Returns: (transfer full): parsed data as key–value pairs
 * Since: UNRELEASED
 */
GHashTable *
thornbury_parser_parse_prop_resource (const gchar *path)
{
  g_autoptr (JsonParser) parser = NULL;
  g_autoptr (GInputStream) stream = NULL;
  g_autoptr (GError) error = NULL;
  g_autoptr (GHashTable) output = NULL;

  g_return_val_if_fail (path != NULL, NULL);

  parser = json_parser_new ();
  stream = g_resources_open_stream (path, G_RESOURCE_LOOKUP_FLAGS_NONE, &error);

  if (error != NULL)
    {
      g_error ("Error parsing prop resource ‘%s’: %s", path, error->message);
      return NULL;
    }

  if (!json_parser_load_from_stream (parser, stream, NULL, &error))
    {
      g_error ("Error parsing prop resource ‘%s’: %s", path, error->message);
      return NULL;
    }

  output = thornbury_parser_parse_json_file (parser, v_parser_iter_prop_node,
                                             &error);

  if (error != NULL)
    {
      g_error ("Error parsing prop resource ‘%s’: %s", path, error->message);
      return NULL;
    }

  return g_steal_pointer (&output);
}
