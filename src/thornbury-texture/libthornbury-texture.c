/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* libthornbury-texture.c
 *
 *  libthornbury-texture.c
 */ 



#include "libthornbury-texture.h"
#include <gtk/gtk.h>

/***********************************************************************************
        @Macro Definitions
************************************************************************************/
#define THORNBURY_TEXTURE_DEBUG(...)   //g_print( __VA_ARGS__)
#define THORNBURY_TEXTURE_ERROR (thornbury_texture_error_quark())

typedef struct _AsyncData AsyncData;

/************************************************************************************
 * Description of Fixes/Functionality:
-----------------------------------------------------------------------------------
        Description                             Date                    Name
        ----------                              ----                    ----
1) GFile and Content freed to reduce 		24-Mar-2014				Abhiruchi
   mem leak									
************************************************************************************/

/**
 * THORBURY_TEXTURE_RGB_BITS_PER_SAMPLE:
 *
 * A macro that is used to set the bits per sample for the RGB colorspace
 *
 * Since: UNRELEASED
 */
# define THORBURY_TEXTURE_RGB_BITS_PER_SAMPLE 8

static gboolean texture_cb (gpointer pData);

static gchar *
thornbury_texture_get_icon_theme_path (const gchar *icon_name);

static GQuark
thornbury_texture_error_quark (void)
{
	return g_quark_from_static_string("thornbury_texture_error_quark");
}
	
/* thread data */
struct _AsyncData
{
	gfloat flWidth;
	gfloat flHeight;

	gchar *pFilePath;
	GError **pErr;
	gpointer pUserData;
	GdkPixbuf *pixbuf;
	thornbury_image_created_cb image_cb;//void (*image_created) (ClutterActor *pimage, GError *pError, gpointer pUserData);
	content_created_cb content_cb;//void (*image_created) (ClutterActor *pimage, GError *pError, gpointer pUserData);
};

static GdkPixbuf *
create_texture_from_resource (const gchar *path,
                              gfloat width,
                              gfloat height,
                              GError **error)
{
  GdkPixbuf *pixbuf = NULL;

  g_return_val_if_fail (path != NULL, NULL);

  g_debug (G_STRLOC "from resource %s", path);

  if (width == 0 || height == 0)
    {
      pixbuf = gdk_pixbuf_new_from_resource (path, error);
    }
  else
    {
      pixbuf = gdk_pixbuf_new_from_resource_at_scale (path, width, height, FALSE, error);
    }

  return pixbuf;
}

/********************************************************
 * function : v_create_texture
 * description: texture creation
 * parameters:  gchar*, gfloat, gfloat, GError**
 * return value: GdkPixbuf*
 ********************************************************/
static GdkPixbuf *v_create_texture(gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError **pErr)
{
	GdkPixbuf *pixbuf = NULL;
        gchar *pUri = NULL;
        GFile *pFile;

	if(NULL == pFilePath)
	{
		if( NULL != pErr)
		{
			/* emit error signal */
			//g_print("\n 1 pError is not null \n");
			g_set_error_literal(pErr, THORNBURY_TEXTURE_ERROR, 0, "File not found");
			return NULL;
		}
		else
		{
			g_warning("File not found\n");
			return NULL;
		}
	}

        pFile = g_file_new_for_commandline_arg (pFilePath);
        if (pFile == NULL)
	{
		 if(NULL != pErr)
        	{
			/* fill error message */
			g_set_error_literal(pErr, THORNBURY_TEXTURE_ERROR, 0, "File not found");
                	return NULL;
		}
		else
		{
			g_warning("File not found\n");
			return NULL;
		}
        }
        if (g_file_has_uri_scheme (pFile, "file"))
        {
                pUri = g_file_get_path (pFile);
	}
	else
	{
		pUri = g_strdup(pFilePath);
	}

	THORNBURY_TEXTURE_DEBUG("%s %f %f\n", pUri, flWidth, flHeight);

	if(flWidth == 0 || flHeight == 0)
	{ 
               pixbuf = gdk_pixbuf_new_from_file(pUri, pErr);
	}
	else
	{
               pixbuf = gdk_pixbuf_new_from_file_at_scale (pUri, flWidth, flHeight, FALSE, pErr);
	}

	if(NULL != pUri)
	{
		g_free (pUri);
		pUri = NULL;	
	}

	if(pFile)
		g_object_unref(pFile);

	return pixbuf;
}

/********************************************************
 * function : v_set_content_data
 * description: setting the image data for content
 * parameters:  ClutterContent*, GdkPixbuf*, GError**
 * return value: gboolean(handled or not)
 ********************************************************/
static void v_set_content_data(ClutterContent *pImage, GdkPixbuf *pixbuf, GError **pErr)
{
	THORNBURY_TEXTURE_DEBUG("%s\n", __FUNCTION__);
	clutter_image_set_data(CLUTTER_IMAGE (pImage),
                                gdk_pixbuf_get_pixels (pixbuf),
                                gdk_pixbuf_get_has_alpha (pixbuf)? COGL_PIXEL_FORMAT_RGBA_8888 : COGL_PIXEL_FORMAT_RGB_888,
                                gdk_pixbuf_get_width (pixbuf),
                                gdk_pixbuf_get_height (pixbuf),
                                gdk_pixbuf_get_rowstride (pixbuf),
                                pErr);
}

/********************************************************
 * function : texture_cb
 * description: texture creation thread callback
 * parameters:  gpointer (pThreadData*)
 * return value: gboolean(handled or not)
 ********************************************************/
static gboolean
texture_cb (gpointer pData)
{
	AsyncData *pThreadData = (AsyncData *)pData;
	ClutterContent *pImage = NULL;
	ClutterActor *pBox = NULL;
	GError *err = NULL;
	
	if(NULL != pThreadData->pixbuf && NULL == *(pThreadData->pErr))
        {
		/* create content */
		pImage = clutter_image_new ();
		v_set_content_data(pImage, pThreadData->pixbuf, pThreadData->pErr);
	        if(NULL == *(pThreadData->pErr))
        	{
			/* set content for the box */
			pBox = clutter_actor_new ();	
                	clutter_actor_set_content (pBox, pImage);
	                clutter_actor_set_size (pBox, gdk_pixbuf_get_width (pThreadData->pixbuf), gdk_pixbuf_get_height (pThreadData->pixbuf));
			g_object_unref(pImage);
        	}
	}

	/* free pixbuf */
        if(G_IS_OBJECT(pThreadData->pixbuf))
	{
                g_object_unref (pThreadData->pixbuf);
		pThreadData->pixbuf = NULL;
	}

	/* free file path */
	if(NULL != pThreadData->pFilePath)
	{
		g_free(pThreadData->pFilePath);
		pThreadData->pFilePath = NULL;
		THORNBURY_TEXTURE_DEBUG("free path\n");
	}

	if(NULL != *(pThreadData->pErr))
	{	
		//g_print("%s\n", (*(pThreadData->pErr))->message);
		g_set_error_literal(&err, THORNBURY_TEXTURE_ERROR, 0, (*(pThreadData->pErr))->message);
	}

	/* callback */
	if(NULL != pThreadData->image_cb)
	        pThreadData->image_cb(pBox, err, pThreadData->pUserData);

	if(NULL != *(pThreadData->pErr))
	{
		g_error_free(*(pThreadData->pErr));
		*(pThreadData->pErr) = NULL;
	}
	/* free thread struct pointer */
	if(NULL != pThreadData)
	{
		g_free(pThreadData);
		pThreadData = NULL;
	}

	/* return from timeout */
        return FALSE;
}

/********************************************************
 * function : create_texture_thread
 * description: texture creation in a thread 
 * parameters:  gpointer (pThreadData*)
 * return value: 
 ********************************************************/
static void create_texture_thread(gpointer pData)
{
        AsyncData *pThreadData = (AsyncData *)pData;

	THORNBURY_TEXTURE_DEBUG("In Thread create pixbuf \n");
	pThreadData->pixbuf = v_create_texture(pThreadData->pFilePath, pThreadData->flWidth, pThreadData->flHeight, pThreadData->pErr);

	/* come out of thread to load content data */
	g_timeout_add(10, (GSourceFunc)texture_cb, pThreadData);
}

/********************************************************
 * function : content_cb
 * description: content creation thread callback
 * parameters:  gpointer
 * return value: gboolean(handled or not)
 ********************************************************/
static gboolean content_cb(gpointer pData)
{
        AsyncData *pThreadData = (AsyncData *)pData;
	GError *err = NULL;
        ClutterContent *pImage = NULL;

        THORNBURY_TEXTURE_DEBUG("out of thread\n");

        if(NULL != pThreadData->pixbuf && (NULL != pThreadData->pErr && NULL == *(pThreadData->pErr)) )
        {
                /* create content */
                pImage = clutter_image_new ();
                v_set_content_data(pImage, pThreadData->pixbuf, pThreadData->pErr);
        }

        /* free pixbuf */
        if(G_IS_OBJECT(pThreadData->pixbuf))
        {
                g_object_unref (pThreadData->pixbuf);
                pThreadData->pixbuf = NULL;
        }

        /* free file path */
        if(NULL != pThreadData->pFilePath)
        {
                g_free(pThreadData->pFilePath);
                pThreadData->pFilePath = NULL;
                THORNBURY_TEXTURE_DEBUG("free path\n");
        }

	if(NULL != pThreadData->pErr && NULL != *(pThreadData->pErr))
	{
	        //g_print("%s\n", (*(pThreadData->pErr))->message);
        	g_set_error_literal(&err, THORNBURY_TEXTURE_ERROR, 0, (*(pThreadData->pErr))->message);
	}	

       	/* callback */
	if(NULL != pThreadData->content_cb)
		pThreadData->content_cb(pImage, err, pThreadData->pUserData);

	if(NULL != pThreadData->pErr && NULL != *(pThreadData->pErr))
	{
		g_error_free(*(pThreadData->pErr));
		*(pThreadData->pErr) = NULL;
	}

        /* free thread struct pointer */
        if(NULL != pThreadData)
        {
                g_free(pThreadData);
                pThreadData = NULL;
        }
        /* return from timeout */
        return FALSE;
}

/********************************************************
 * function : create_texture_content_thread
 * description: texture creation in a thread 
 * parameters:  gpointer (pThreadData*)
 * return value: 
 ********************************************************/
static void create_texture_content_thread(gpointer pData)
{
        AsyncData *pThreadData = (AsyncData *)pData;

        THORNBURY_TEXTURE_DEBUG("In Thread create pixbuf \n");
        pThreadData->pixbuf = v_create_texture(pThreadData->pFilePath, pThreadData->flWidth, pThreadData->flHeight, pThreadData->pErr);

        /* come out of thread to load content data */
        g_timeout_add(10, (GSourceFunc)content_cb, pThreadData);
}

/**
 * thornbury_texture_set_from_file:
 * @pBox: actor for which new image b=needs to be set 
 * @pFilePath: Path of the new image file 
 * @flWidth: The width of the texture
 * @flHeight: The height of the texture
 * @pErr:   GError pointer which will get filled with proper error  if occurred
 *	
 * Updates the given ClutterActor with the given image file of given
 * height and width. Also the scaling is done to given width
 * and height if width and height are non zero numbers)
 *
 */
void thornbury_texture_set_from_file (ClutterActor *pBox, gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError **pErr)
{
	GdkPixbuf *pixbuf = NULL;
	ClutterContent *pImage = NULL;

	/* if image actor or file path is null, return */
        if ( pBox == NULL || pFilePath == NULL) 
	{
		if(NULL != pErr)
        	{
                	//g_warning ("%s: Path/Image Actor is NULL\n", __FUNCTION__);
			g_set_error_literal(pErr, THORNBURY_TEXTURE_ERROR, 0, "Path/Image Actor is NULL");
                	return;
        	}
		else
		{
			g_warning ("%s: Path/Image Actor is NULL\n", __FUNCTION__);
			return;
		}
	}

        pixbuf = v_create_texture(pFilePath, flWidth, flHeight, pErr);

	pImage = clutter_actor_get_content(pBox);	
	if(! CLUTTER_IS_IMAGE(pImage))
	{
		if( NULL != pErr)
        	{
                	//g_warning("not a image actor\n");
			g_set_error_literal(pErr, THORNBURY_TEXTURE_ERROR, 0, "CLUTTER_IS_IMAGE failed");
                	return;
        	}
		else
		{
			g_warning("not a image actor\n");
			return;
		}	
	}
	v_set_content_data(pImage, pixbuf, pErr);
	if( (NULL == pErr) || (NULL != pErr && NULL == *pErr) )
	{
		/* update the content box with image size and set content to it */
        	clutter_actor_set_size (pBox, gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf));
	}

        if(G_IS_OBJECT(pixbuf))
                g_object_unref (pixbuf);
}

/**
 * thornbury_texture_set_from_resource:
 * @box: actor for which new image needs to be set
 * @path: Gresource Path of the new image file
 * @width: The width of the texture
 * @height: The height of the texture
 * @error:   GError pointer which will get filled with proper error  if occurred
 *      
 * Updates the given ClutterActor with the given image file of given
 * height and width. Also the scaling is done to given width
 * and height if width and height are non zero.
 */
void
thornbury_texture_set_from_resource (ClutterActor *box,
                                     const gchar *path,
                                     gfloat width,
                                     gfloat height,
                                     GError **error)
{
  GError *tmp_error = NULL;
  GdkPixbuf *pixbuf = NULL;
  ClutterContent *image = NULL;

  g_return_if_fail (CLUTTER_IS_ACTOR (box));
  g_return_if_fail (path != NULL);
  g_return_if_fail (error == NULL || *error == NULL);

  pixbuf = create_texture_from_resource (path, width, height, &tmp_error);

  if (NULL != tmp_error)
    {
      g_propagate_error (error, tmp_error);
      return;
    }
  image = clutter_actor_get_content (box);
  v_set_content_data (image, pixbuf, &tmp_error);

  if (NULL != tmp_error)
    {
      g_clear_object (&pixbuf);
      g_propagate_error (error, tmp_error);
      return;
    }

  /* update the content box with image size and set content to it */
  clutter_actor_set_size (box, gdk_pixbuf_get_width (pixbuf),
                          gdk_pixbuf_get_height (pixbuf));

  g_clear_object (&pixbuf);
}

/**
 * thornbury_texture_create_sync:
 * @pFilePath: Path of the new image file 
 * @flWidth: The width of the texture
 * @flHeight: The height of the texture
 * @pErr: GError pointer which will get filled with proper error  if occurred
 *
 * Create a ClutterActor from the given image file with given
 * height and width.Also the scaling is done to width
 * and height if width and height are non zero numbers.
 *
 * Returns: The newly created #ClutterActor having the #ClutterImage
 *          with properties as specified
 */
ClutterActor *thornbury_texture_create_sync (gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError **pErr)
{
	GdkPixbuf *pixbuf = NULL;
	ClutterContent *pImage = NULL;
	ClutterActor *pBox = NULL;

	pixbuf = v_create_texture(pFilePath, flWidth, flHeight, pErr);
	if(NULL != pixbuf && ( (pErr != NULL && *pErr == NULL) || NULL == pErr) )
	{
		pImage = clutter_image_new ();
		v_set_content_data(pImage, pixbuf, pErr);
		if(NULL == pErr || (pErr != NULL && NULL == *pErr))
		{
			pBox = clutter_actor_new ();
			clutter_actor_set_content (pBox, pImage);
			clutter_actor_set_size (pBox, gdk_pixbuf_get_width (pixbuf), gdk_pixbuf_get_height (pixbuf));
			g_object_unref(pImage);
		}
		if(G_IS_OBJECT(pixbuf))
			g_object_unref (pixbuf);
	}
	return pBox;
}

/**
 * thornbury_texture_create_from_resource_sync:
 * @path: Gresource Path of the new image file
 * @width: The width of the texture
 * @height: The height of the texture
 * @error: GError pointer which will get filled with proper error  if occurred
 *
 * Create a ClutterActor from the given image file with given
 * height and width.Also the scaling is done to width
 * and height if width and height are non zero numbers.
 * Returns: (transfer full): The newly created #ClutterActor having the #ClutterImage
 *          with properties as specified
 */
ClutterActor *
thornbury_texture_create_from_resource_sync (const gchar *path,
                                             gfloat width,
                                             gfloat height,
                                             GError **error)
{
  GError *tmp_error = NULL;
  GdkPixbuf *pixbuf = NULL;
  ClutterContent *image = NULL;
  ClutterActor *box = NULL;

  g_return_val_if_fail (path != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  pixbuf = create_texture_from_resource (path, width, height, &tmp_error);
  if (NULL != tmp_error)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  image = clutter_image_new ();
  v_set_content_data (image, pixbuf, &tmp_error);

  if (NULL != tmp_error)
    {
      g_object_unref (image);
      g_clear_object (&pixbuf);
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  box = clutter_actor_new ();
  clutter_actor_set_content (box, image);
  clutter_actor_set_size (box, gdk_pixbuf_get_width (pixbuf),
                          gdk_pixbuf_get_height (pixbuf));
  g_object_unref (image);
  g_clear_object (&pixbuf);
  return box;
}

/**
 * thornbury_texture_create_async:
 * @pFilePath: Path of the new image file 
 * @flWidth:  The width of the texture
 * @flHeight: The height of the texture
 * @pData:    user data
 * @pFunc: callback function pointer 
 *
 * Create a ClutterActor from the given image file with given
 * height and width in async way.Also the scaling is done to width
 * and height if width and height are non zero numbers.
 * Created texture will be returned in the given callback function with 
 * provided user data.
 *
 */
void thornbury_texture_create_async (gchar *pFilePath, gfloat flWidth, gfloat flHeight, gpointer pData, thornbury_image_created_cb pFunc)
{
	static GError *pErr = NULL;
	if(NULL != pErr)
	{
		g_clear_error(&pErr);
		pErr = NULL;
	}

	if(NULL != pFilePath)	
	{
			AsyncData *pThreadData = g_new0(AsyncData, 1);
			pThreadData->pFilePath = g_strdup(pFilePath);
			pThreadData->flWidth = flWidth;
			pThreadData->flHeight = flHeight;
			pThreadData->image_cb = pFunc;
			pThreadData->pUserData = pData;
			pThreadData->pErr = &pErr;
			pThreadData->pixbuf = NULL;
			g_thread_new("texture-thread", (GThreadFunc)create_texture_thread, pThreadData);
	}
}

/**
 * thornbury_texture_create_content_sync:
 * @pFilePath: Path of the new image file 
 * @flWidth:  The width of the texture
 * @flHeight: The height of the texture
 * @pErr:     GError pointer which will get filled with proper error  if occurred
 *
 * Create a ClutterContent from the given image file with given
 * height and width.Also the scaling is done to width
 * and height if width and height are non zero numbers)
 *
 * Returns: The newly created #ClutterContent which needs to be added
 *          to desired actor using clutter_actor_set_content.
 *
 */
ClutterContent *thornbury_texture_create_content_sync (gchar *pFilePath, gfloat flWidth, gfloat flHeight, GError **pErr)
{
	GdkPixbuf *pixbuf = NULL;
        ClutterContent *pImage = NULL;

	pixbuf = v_create_texture(pFilePath, flWidth, flHeight, pErr);
	if(NULL != pixbuf && ( (pErr != NULL && *pErr == NULL) || NULL == pErr) )
	{
		pImage = clutter_image_new ();
		v_set_content_data(pImage, pixbuf, pErr);
		if(G_IS_OBJECT(pixbuf))
                        g_object_unref (pixbuf);

	}        

	//if(*pErr && (*pErr)->message)
	//	g_print("%s\n", (*pErr)->message);

	return pImage;
}

/**
 * thornbury_texture_create_content_from_resource_sync:
 * @path: Path of the new image file
 * @width:  The width of the texture
 * @height: The height of the texture
 * @error:     GError pointer which will get filled with proper error  if occurred
 *
 * Create a ClutterContent from the given image file with given
 * height and width.Also the scaling is done to width
 * and height if width and height are non zero numbers)
 *
 * Returns: (transfer full): The newly created #ClutterContent which needs to be added
 *          to desired actor using clutter_actor_set_content.
 */
ClutterContent *
thornbury_texture_create_content_from_resource_sync (const gchar *path,
                                                     gfloat width,
                                                     gfloat height,
                                                     GError **error)
{
  GError *tmp_error = NULL;
  GdkPixbuf *pixbuf = NULL;
  ClutterContent *image = NULL;

  g_return_val_if_fail (path != NULL, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  pixbuf = create_texture_from_resource (path, width, height, &tmp_error);

  if (NULL != tmp_error)
    {
      g_propagate_error (error, tmp_error);
      return NULL;
    }

  image = clutter_image_new ();
  v_set_content_data (image, pixbuf, &tmp_error);
  g_clear_object (&pixbuf);

  if (NULL != tmp_error)
    {
      g_object_unref (image);
      g_propagate_error (error, tmp_error);
      return NULL;
    }
  return image;
}

/**
 * thornbury_texture_create_content_async:
 * @pFilePath: Path of the new image file 
 * @flWidth:  The width of the texture
 * @flHeight: The height of the texture
 * @pData:    user data
 * @content_created_cb: callback function pointer 
 *
 * Create a ClutterContent from the given image file with given
 * height and width in async way.Also the scaling is done to width
 * and height if width and height are non zero numbers).
 * Created content actor will be returned in the given callback function with 
 * provided user data
 *
 */
void thornbury_texture_create_content_async (gchar *pFilePath, gfloat flWidth, gfloat flHeight, gpointer pData, content_created_cb pFunc)
{
	static GError *pErr = NULL;

	if(NULL != pErr)
	{
		g_clear_error(&pErr);
		pErr = NULL;
	}
	if(NULL != pFilePath)
        {

                        AsyncData *pThreadData = g_new0(AsyncData, 1);
                        pThreadData->pFilePath = g_strdup(pFilePath);
                        pThreadData->flWidth = flWidth;
                        pThreadData->flHeight = flHeight;
                        pThreadData->content_cb = pFunc;
                        pThreadData->pUserData = pData;
                        pThreadData->pErr = &pErr;
                        pThreadData->pixbuf = NULL;
                        g_thread_new("texture-thread", (GThreadFunc)create_texture_content_thread, pThreadData);
        }
}

/**
 * thornbury_texture_new:
 *
 * Creates a new ClutterActor for texture 
 *
 * Returns: (transfer full): The newly created #ClutterActor having the #ClutterImage
 */
ClutterActor *
thornbury_texture_new (void)
{
  ClutterContent *pImage = NULL;
  ClutterActor *pBox = NULL;

  g_debug ("%s", __FUNCTION__);

  pImage = clutter_image_new ();
  pBox = clutter_actor_new ();
  clutter_actor_set_content (pBox, pImage);
  g_object_unref (pImage);
  return pBox;
}

/**
 * thornbury_texture_new_from_icon:
 * @icon: a #GIcon containing the icon
 * @width: The width of the texture, must be >= 0
 * @height: The height of the texture, must be >= 0
 *
 * Creates a new ClutterActor for texture through #GIcon
 *
 * Returns: (transfer full): The newly created #ClutterActor having the #ClutterImage
 *
 * Since: UNRELEASED
 */
ClutterActor *
thornbury_texture_new_from_icon (GIcon *icon, gfloat width, gfloat height)
{
  ClutterContent *image = NULL;
  ClutterActor *actor = NULL;
  GError *error = NULL;

  g_return_val_if_fail (width >= 0, NULL);
  g_return_val_if_fail (height >= 0, NULL);

  if (G_IS_FILE_ICON (icon))
    {
      /* This will handle the schemas like file://, https://, resource:// */
      g_autofree gchar *file_path = NULL;

      file_path = g_icon_to_string (G_ICON (icon));

      if (file_path != NULL)
	{
	  actor = thornbury_texture_new ();
	  thornbury_texture_set_from_file (actor, file_path, 0, 0, &error);

	  if (error != NULL)
	    {
	      g_warning ("Failed to load icon from path ‘%s’: %s", file_path,
			 error->message);
	      g_error_free (error);
	      g_clear_pointer (&actor, clutter_actor_destroy);
	    }
	}
    }
  else if (G_IS_BYTES_ICON (icon))
    {
      GBytes *icon_bytes = g_bytes_icon_get_bytes (G_BYTES_ICON (icon));

      /* construct a texture from the pixmap bytes */
      if (icon_bytes != NULL)
	{
	  GdkPixbuf *pix_buf = NULL;

	  /* Currently only RGB images with 8 bits per sample are supported for
	   * gdk_pixbuf_new_from_bytes and rowstride: width*4 */
	  pix_buf = gdk_pixbuf_new_from_bytes (
	      icon_bytes, GDK_COLORSPACE_RGB, TRUE,
	      THORBURY_TEXTURE_RGB_BITS_PER_SAMPLE, width, height, width * 4);

	  if (pix_buf != NULL)
	    {
	      image = clutter_image_new ();
	      v_set_content_data (image, pix_buf, &error);

	      if (error == NULL)
		{
		  actor = clutter_actor_new ();
		  clutter_actor_set_content (actor, image);
		}
	      else
		{
		  g_warning ("Failed to set the data to the clutter-image. %s",
			     error->message);
		  g_error_free (error);
		}
	      g_clear_object (&image);
	      g_clear_object (&pix_buf);
	    }
	  g_clear_pointer (&icon_bytes, g_bytes_unref);

	}
    }
  else if (G_IS_THEMED_ICON (icon))
    {
      const gchar * const *icon_names;
      g_autofree gchar *icon_path = NULL;
      gsize i;

      icon_names = g_themed_icon_get_names (G_THEMED_ICON (icon));
      g_assert (icon_names != NULL && icon_names[0] != NULL);

      for (i = 0; icon_names[i] != NULL && icon_path == NULL; i++)
	{
	  icon_path = thornbury_texture_get_icon_theme_path (icon_names[i]);
	}

      if (icon_path == NULL)
	{
	  /* Don’t load the icon. */
	  g_warning (
	      "Icon ‘%s’ not found in the theme, and no fallbacks were found either. Ignoring.",
	      icon_names[0]);
	  return NULL;
	}
      else
	{
	  actor = thornbury_texture_new ();
	  thornbury_texture_set_from_file (actor, icon_path, 0, 0, &error);

	  if (error != NULL)
	    {
	      g_warning ("Failed to load icon from path ‘%s’: %s", icon_path,
			 error->message);
	      g_error_free (error);
	      g_clear_pointer (&actor, clutter_actor_destroy);
	    }
	  g_clear_pointer (&icon_path, g_free);
	}
    }
  else
    {
      /* Don’t load the icon. */
      g_warning ("Unsupported icon type ‘%s’. Ignoring.",
		 G_OBJECT_TYPE_NAME (icon));
    }
  return actor;
}

/**
 * thornbury_texture_get_stage_screenshot:
 * @pStage: Stage pointer which you want to take the screenshot
 * @inXStartPos: dafdafadf
 * @inYStartPos: fdafd
 * @inWidth: dafd
 * @inHeight: dfafda
 * @pFilename: adsfafda
 *
 * Get the Stage screenshot from the given ClutterStage pointer.
 *
 * Returns: The state TRUE or FALSE
 *
 */
gboolean thornbury_texture_get_stage_screenshot(ClutterStage *pStage, glong inXStartPos, glong inYStartPos, glong inWidth, glong inHeight, gchar *pFilename)
{
	gboolean bPicCreated = FALSE ;
	GError* pError = NULL;
	GdkPixbuf* pScreenShot;
        // extract frame 

	guchar *pBuffer = g_malloc (inWidth * inHeight * 4);
	cogl_framebuffer_read_pixels ( cogl_get_draw_framebuffer(),
			inXStartPos, inYStartPos, inWidth, inHeight,
			COGL_PIXEL_FORMAT_RGBA_8888,
			pBuffer);

#if 0
         Clutter is internally calling paint before reading the buffer which consumes more time.
	 guchar *pBuffer = clutter_stage_read_pixels(
			CLUTTER_STAGE (pStage), inXStartPos, inYStartPos,
			inWidth ,inHeight);
#endif

	pScreenShot = gdk_pixbuf_new_from_data (pBuffer,
						GDK_COLORSPACE_RGB,
						TRUE, 8,
						inWidth, inHeight,
						inWidth * 4,
						(GdkPixbufDestroyNotify) g_free, NULL);
	if(NULL != pScreenShot )
	{
		if(NULL != pFilename)
		{
			gdk_pixbuf_save (pScreenShot, pFilename, "png", &pError,NULL);
			if(NULL != pError)
			{
				g_warning("Error occurred while taking screenshot : Error msg :-> %s ",pError->message);
				g_error_free(pError);
			}
			else
			{
				bPicCreated =  TRUE ;
			}
		}
		g_object_unref(pScreenShot);
	}
	return bPicCreated;
}

static gchar *
thornbury_texture_get_icon_theme_path (const gchar *icon_name)
{
  GtkIconInfo *gtk_icon_info = NULL;
  gchar *file_path = NULL;

  gtk_icon_info = gtk_icon_theme_lookup_icon (
      gtk_icon_theme_get_default (), icon_name, 0, GTK_ICON_LOOKUP_DIR_LTR);
  if (gtk_icon_info == NULL)
    {
      g_debug ("Failed to look up icon for \"%s\"", icon_name);
    }
  else
    {
      file_path = g_strdup (gtk_icon_info_get_filename (gtk_icon_info));
      g_object_unref (gtk_icon_info);
    }
  return file_path;
}
