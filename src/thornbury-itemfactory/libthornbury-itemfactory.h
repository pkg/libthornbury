/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION: thornbury-itemfactory/libthornbury-itemfactory.h
 * @title: ThornburyItemFactory
 */

#ifndef _THORNBURY_ITEM_FACTORY_H
#define _THORNBURY_ITEM_FACTORY_H

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <glib-object.h>
#include "libthornbury-model.h"

G_BEGIN_DECLS

#define THORNBURY_TYPE_ITEM_FACTORY thornbury_item_factory_get_type ()
G_DECLARE_FINAL_TYPE (ThornburyItemFactory, thornbury_item_factory, THORNBURY, ITEM_FACTORY, GObject)

ThornburyItemFactory *thornbury_item_factory_generate_widget_with_props(GType type, const gchar *filepath);
void thornbury_item_factory_apply_model(ThornburyItemFactory *itemFactory, ThornburyModel *model);
ThornburyItemFactory *thornbury_item_factory_generate_widget(GType type);

ThornburyItemFactory *thornbury_item_factory_generate_widget_with_props_from_resource (GType type,
                                                                                       const gchar *path);

G_END_DECLS

#endif /* _THORNBURY_ITEM_FACTORY_H */
