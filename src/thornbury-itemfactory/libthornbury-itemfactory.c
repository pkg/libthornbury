/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * ThornburyItemFactory::ThornburyItemFactory:
 * @Title:ThornburyItemFactory
 * @Short_Description: Thornbury widgets instances are created using Item Factory.
 * @See_Also: #MxItemFactory #ClutterActor
 *
 * #ThornburyItemFactory is meant to create the widget instance for given property rather interacting directly with widget.
 * it is for the convienience of MVC.Item factory is mostly used in #ThornburyViewManager to instantiate the widget using the
 * property .json file. 
 * it is derived from interface class #MxItemFactory, which will overide the (*create)() virtual function. mx_item_factory_create() interface 
 * creates the instance of the type as provided by the property "item-type" during the creation of the itemfactory instance.
 * 
 * we can create the item factory instance in two ways, as shown below.
 * 
 *
 * ## Sample C Code
 * |[<!-- language="C" -->
 * item_factory = g_object_new (TYPE_THORNBURY_ITEM_FACTORY,
 *                               "item-type", item_type,//type of the widget
 *                              "model", model, //model for item (optional)
 *                              NULL);
 * //or 
 * item_factory = thornbury_item_factory_generate_widget_with_props(type,widgetpath);
 * ]|
*/
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wstrict-prototypes"
#include <mx/mx.h>
#pragma GCC diagnostic pop

#include "libthornbury-itemfactory.h"
#include "libthornbury-ui-utility.h"
#include "libthornbury-widgetparser.h"

static void mx_item_factory_iface_init (MxItemFactoryIface *iface);

struct _ThornburyItemFactory
{
  GObject parent;
};

typedef struct
{
	GType item_type;
	GObject *object;
	ThornburyModel *model;
} ThornburyItemFactoryPrivate;

G_DEFINE_TYPE_WITH_CODE (ThornburyItemFactory, thornbury_item_factory, G_TYPE_OBJECT,
                         G_ADD_PRIVATE (ThornburyItemFactory)
                         G_IMPLEMENT_INTERFACE (MX_TYPE_ITEM_FACTORY, mx_item_factory_iface_init))

enum {
	PROP_0,
	PROP_ITEM_TYPE,
	PROP_MODEL,
	PROP_OBJECT
};

static void set_model (ThornburyItemFactory *itemfactory, ThornburyModel *model);

static ClutterActor* item_factory_create (MxItemFactory *factory)
{
	ThornburyItemFactory *self = THORNBURY_ITEM_FACTORY (factory);
	ThornburyItemFactoryPrivate *priv = thornbury_item_factory_get_instance_private (self);
	ClutterActor *item;

	item = g_object_new (priv->item_type, NULL);

	if(G_IS_OBJECT(item))
	{
		priv->object = (GObject *)item;

		/* If the newly created object has model as a property, submit
		 * the model to the item */
		if (priv->model && g_object_class_find_property (G_OBJECT_GET_CLASS (item), "model") != NULL)
			g_object_set (item, "model", priv->model, NULL);
	}

	return item;
}

static void mx_item_factory_iface_init (MxItemFactoryIface *iface)
{
	iface->create = item_factory_create;
}


static void thornbury_item_factory_get_property (GObject    *object,
		guint       property_id,
		GValue     *value,
		GParamSpec *pspec)
{
	ThornburyItemFactory *self = THORNBURY_ITEM_FACTORY (object);
	ThornburyItemFactoryPrivate *priv = thornbury_item_factory_get_instance_private (self);

	switch (property_id)
	{
		case PROP_ITEM_TYPE:
			g_value_set_gtype (value, priv->item_type);
			break;
		case PROP_MODEL:
			g_value_set_object (value, priv->model);
			break;
		case PROP_OBJECT:
			g_value_set_object (value, priv->object);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void set_model(ThornburyItemFactory *itemFactory, ThornburyModel *model)
{
  ThornburyItemFactoryPrivate *priv = thornbury_item_factory_get_instance_private (itemFactory);

  g_assert (model != NULL);

  if (priv->model == model)
    return;

  g_clear_object (&priv->model);
  priv->model = g_object_ref (model);

  if (priv->object != NULL &&
      g_object_class_find_property (G_OBJECT_GET_CLASS (priv->object), "model") != NULL)
    g_object_set (priv->object, "model", priv->model, NULL);

  g_object_notify (G_OBJECT (itemFactory), "model");
}

static void thornbury_item_factory_set_property (GObject      *object,
		guint         property_id,
		const GValue *value,
		GParamSpec   *pspec)
{
	ThornburyItemFactory *self = THORNBURY_ITEM_FACTORY (object);
	ThornburyItemFactoryPrivate *priv = thornbury_item_factory_get_instance_private (self);

	switch (property_id)
	{
		case PROP_ITEM_TYPE:
			priv->item_type = g_value_get_gtype (value);
			break;
		case PROP_MODEL:
			set_model (self, g_value_get_object (value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
	}
}

static void thornbury_item_factory_dispose (GObject *object)
{
  ThornburyItemFactory *self = THORNBURY_ITEM_FACTORY (object);
  ThornburyItemFactoryPrivate *priv = thornbury_item_factory_get_instance_private (self);

  g_clear_object (&priv->model);

  G_OBJECT_CLASS (thornbury_item_factory_parent_class)->dispose (object);
}

static void thornbury_item_factory_finalize (GObject *object)
{
	G_OBJECT_CLASS (thornbury_item_factory_parent_class)->finalize (object);
}

static void thornbury_item_factory_class_init (ThornburyItemFactoryClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GParamSpec *pspec;

	object_class->get_property = thornbury_item_factory_get_property;
	object_class->set_property = thornbury_item_factory_set_property;
	object_class->dispose = thornbury_item_factory_dispose;
	object_class->finalize = thornbury_item_factory_finalize;

	pspec = g_param_spec_gtype ("item-type",
			"Item GType",
			"Item GType to instantiate",
			CLUTTER_TYPE_ACTOR,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_ITEM_TYPE, pspec);

	pspec = g_param_spec_object ("model",
			"ThornburyModel",
			"ThornburyModel",
			G_TYPE_OBJECT,
			G_PARAM_READWRITE);
	g_object_class_install_property (object_class, PROP_MODEL, pspec);

	pspec = g_param_spec_object ("object",
			"Object",
			"Object created of item-type",
			G_TYPE_OBJECT,
			G_PARAM_READABLE);
	g_object_class_install_property (object_class, PROP_OBJECT, pspec);

}

static void
thornbury_item_factory_init (ThornburyItemFactory *self)
{
}

static void apply_widget_properties (gpointer key, gpointer value, gpointer userData)
{
	if(G_IS_OBJECT(userData) && (NULL != key) && (NULL != value) )
	{
		//guint propType = G_VALUE_TYPE(value);
		GObject *object = (GObject *)userData;
		gchar *propName = (gchar *)key;
		
		if (g_object_class_find_property (G_OBJECT_GET_CLASS (object), propName) == NULL)
                           return;
        
                g_object_set_property (G_OBJECT (object), propName, value);
# if 0
		//g_print("item factory %s\n", propName);
		switch(propType)
		{
			case G_TYPE_BOOLEAN:
				{
					gboolean bVal = g_value_get_boolean(value);
					g_object_set(object, propName, bVal, NULL);
					break;
				}
			case G_TYPE_INT:
				{
					gint inVal = g_value_get_int(value);
					g_object_set(object, propName, inVal, NULL);
					break;
				}
			case G_TYPE_INT64:
				{
					gint64 inVal = g_value_get_int64(value);
					g_object_set(object, propName, inVal, NULL);
					break;
				}

			case G_TYPE_UINT:
				{
					guint uinVal = g_value_get_uint(value);
					g_object_set(object, propName, uinVal, NULL);

					break;
				}
			case G_TYPE_UINT64:
				{
					guint64 ulnVal = g_value_get_uint64(value);
					g_object_set(object, propName, ulnVal, NULL);

					break;
				}
			case G_TYPE_LONG:
				{
					glong lnVal = g_value_get_long(value);
					g_object_set(object, propName, lnVal, NULL);

					break;
				}
			case G_TYPE_ULONG:
				{
					gulong ulnVal = g_value_get_ulong(value);
					g_object_set(object, propName, ulnVal, NULL);

					break;
				}
			case G_TYPE_FLOAT:
				{
					gfloat fltVal = g_value_get_float(value);
					g_object_set(object, propName, fltVal, NULL);

					break;
				}
			case G_TYPE_STRING:
				{
					const gchar* pVal = g_strdup(g_value_get_string(value));
					if(g_strcmp0(key, "color") == 0)
					{
						ClutterColor color = {0};
						clutter_color_from_string (&color, pVal);
						g_object_set(object, propName, &color, NULL);
					}
					else
						g_object_set(object, propName, pVal, NULL);
					break;
				}
			case G_TYPE_DOUBLE:
				{
					gdouble dVal = g_value_get_double(value);
					g_object_set(object, propName, dVal, NULL);
					break;
				}
			case G_TYPE_POINTER:
				{
					gpointer pVal = g_value_get_pointer(value);
					if(g_strcmp0(key, "color") == 0)
					{
						ClutterColor *color;
						color = (ClutterColor *)pVal;
						if(color)
							g_object_set(object, propName, color, NULL);
					}
					break;
				}
			case G_TYPE_ENUM:
				{
					guint enumVal = g_value_get_enum(value);
					g_object_set(object, propName, enumVal, NULL);
					break;
				}
			case G_TYPE_FLAGS:
				{
					guint flags = g_value_get_flags(value);
					g_object_set(object, propName, flags, NULL);
					break;
				}
			case G_TYPE_CHAR:
				{
					gchar charVal = g_value_get_schar(value);
					g_object_set(object, propName, &charVal, NULL);
					break;
				}
			case G_TYPE_UCHAR:
				{
					guchar uCharVal = g_value_get_uchar(value);
					g_object_set(object, propName, &uCharVal, NULL);
					break;
				}
			case G_TYPE_BOXED:
				{
					guchar uCharVal = g_value_get_uchar(value);
					g_object_set(object, propName, &uCharVal, NULL);
					break;
				}
			case G_TYPE_OBJECT:
				{
					gpointer obj = g_value_get_object(value);
					g_object_set(object, propName, obj, NULL);
					break;
				}
			case G_TYPE_PARAM:
				{
					GParamSpec *spec = g_value_get_param(value);
					g_object_set(object, propName, spec, NULL);
					break;
				}
			case G_TYPE_VARIANT:
				{
					GVariant *variant = g_value_get_variant(value);
					g_object_set(object, propName, variant, NULL);
					break;
				}
			case G_TYPE_INVALID:
			case G_TYPE_NONE:
			case G_TYPE_INTERFACE:
			default:
				g_warning("Invalid property type. Cannot set widget property \n");
				break;
		}
# endif
	}
}

# if 0
static void parse_widget_properties (gpointer key, gpointer value, gpointer userData)
{
	if(IS_THORNBURY_ITEM_FACTORY(userData) && (NULL != key) && (NULL != value) )
	{
		ThornburyItemFactoryPrivate *priv = THORNBURY_ITEM_FACTORY (userData)->priv;

		ThornburyWidgetParserData *widgetData = (ThornburyWidgetParserData *)value;

		if( (G_TYPE_OBJECT == widgetData->uinType) && (NULL != widgetData->pObjData))
		{
			ThornburyWidgetObjectData *objectData = (ThornburyWidgetObjectData *)(widgetData->pObjData);

			GObject *newObject = g_object_new(objectData->uinObjectType, NULL);
			if( (G_IS_OBJECT(newObject)) && (NULL != objectData->pPropertyHash) )
				g_hash_table_foreach(objectData->pPropertyHash, apply_widget_properties, newObject);
			/* set the object as a property to the widget */
			if(G_IS_OBJECT(newObject))
				g_object_set(priv->object, widgetData->propName, newObject, NULL);
		}
		else if(widgetData->uinType == G_TYPE_GTYPE)
		{
			const gchar* pVal = g_strdup(g_value_get_string(widgetData->pValue) );
			g_print("%s\n", g_type_name(get_type_from_class(pVal)));
			g_object_set(priv->object, widgetData->propName, get_type_from_class(pVal), NULL);
		}
		else	
			apply_widget_properties(widgetData->propName, widgetData->pValue, priv->object);
	}
}
# endif

static void
parse_widget_properties (ThornburyWidgetParserData *widgetData, ThornburyItemFactory *itemFactory)
{ 

	ThornburyItemFactoryPrivate *priv = thornbury_item_factory_get_instance_private (itemFactory);

	g_return_if_fail (widgetData != NULL);

	if( (G_TYPE_OBJECT == widgetData->uinType) && (NULL != widgetData->pObjData))
        {
                ThornburyWidgetObjectData *objectData = (ThornburyWidgetObjectData *)(widgetData->pObjData);

                GObject *newObject = g_object_new(objectData->uinObjectType, NULL);
                if( (G_IS_OBJECT(newObject)) && (NULL != objectData->pPropertyHash) )
                        g_hash_table_foreach(objectData->pPropertyHash, apply_widget_properties, newObject);
                /* set the object as a property to the widget */
                if(G_IS_OBJECT(newObject))
                        g_object_set(priv->object, widgetData->propName, newObject, NULL);
        }
        else if(widgetData->uinType == G_TYPE_GTYPE)
	{
		const gchar* value;
		GType type_value;

		value = g_value_get_string (widgetData->pValue);
		type_value = thornbury_ui_utility_get_gtype_from_name (value);

		g_object_set(priv->object,
			widgetData->propName, type_value,
			NULL);
	}
        else
	{
                apply_widget_properties(widgetData->propName, widgetData->pValue, priv->object);
	}
}
/**
 * thornbury_item_factory_generate_widget:
 * @type :type of the widget
 *
 * This Function will create the instance of the item factory which can create a widget of type @type,
 * mx_item_factory_create(ThornburyItemFactory *) will create the widget.
 *
 * Returns: (transfer full): instance of item factory
 *
 * since 1.0.
 * 
 */

ThornburyItemFactory *thornbury_item_factory_generate_widget(GType type)
{
	return thornbury_item_factory_generate_widget_with_props(type, NULL);
}

/**
 * thornbury_item_factory_apply_model:
 * @itemFactory : Pointer to itemfactory.
 * @model : pointer to #ThornburyModel
 * 
 * sets the model to the widget, whenever created.
 * Note: This has to be called only after thornbury_item_factory_generate_widget()
 *
 * since 1.0.
 *
 */
void thornbury_item_factory_apply_model(ThornburyItemFactory *itemFactory, ThornburyModel *model)
{
  if (THORNBURY_IS_ITEM_FACTORY (itemFactory) && G_IS_OBJECT (model))
    set_model (itemFactory, model);
}

static gint
sort_prop (gconstpointer a, gconstpointer b)
{
	return g_strcmp0 ((gchar *) a, (gchar*) b);
}

static ThornburyItemFactory *
generate_widget_with_props (GType       type,
                            GHashTable *props)
{
	ThornburyItemFactory* item_factory;
	GList *keys, *tmp;

	item_factory = g_object_new (THORNBURY_TYPE_ITEM_FACTORY,
		"item-type", type,
		NULL);

	// FIXME: Resource leakage!
	// If item factory is not created here, no menu is displayed
	// in simulator. However, this also lost a chance to be destroyed.
	mx_item_factory_create (MX_ITEM_FACTORY (item_factory));

	if (props == NULL)
		return item_factory;

	keys = g_hash_table_get_keys (props);
	keys = g_list_sort (keys, (GCompareFunc) sort_prop);

	for (tmp = keys; tmp; tmp = tmp->next)
	{
                ThornburyWidgetParserData *parsed_data;

		parsed_data = g_hash_table_lookup (props, tmp->data);
		parse_widget_properties (parsed_data, item_factory);
	}

	g_list_free (keys);

	return item_factory;
}

/**
 * thornbury_item_factory_generate_widget_with_props:
 * @type: type of the widget
 * @filepath: file path for widget
 *
 * This Function will create the instance of the item factory which will create a widget using json file
 * of type
 * ::object will get the object created.
 *
 * Returns: (transfer full): Instance of #ThornburyItemFactory
 *
 * since 1.0.
 *
 */
ThornburyItemFactory *
thornbury_item_factory_generate_widget_with_props (GType type, const gchar *filepath)
{
	GHashTable *props = NULL;
	ThornburyItemFactory *factory = NULL;

	if (filepath != NULL)
		props = thornbury_parser_parse_prop_file (filepath);
	factory = generate_widget_with_props (type, props);

	g_clear_pointer (&props, thornbury_parser_free_hash);

	return factory;
}

ThornburyItemFactory *
thornbury_item_factory_generate_widget_with_props_from_resource (GType type,
                                                                 const gchar *path)
{
	GHashTable *props;
	ThornburyItemFactory *factory = NULL;

	props = thornbury_parser_parse_prop_resource (path);
	factory = generate_widget_with_props (type, props);
	thornbury_parser_free_hash (props);

	return factory;
}
