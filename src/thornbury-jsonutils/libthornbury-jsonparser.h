/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/**
 * SECTION:thornbury-jsonutils/libthornbury-jsonparser.h
 * @title: ThornburyJsonParser
 * @short_description: Json parser for Thornbury
 *
 * This is the json parser for parsing the json file,typically used in #ThornburyViewManager
 * to parse view json files in an application to create views. This view contains view id is 
 * unique in an application and set of widget to be created in the views. 
 * This is using Yajl library to parse the json files.
 */

#ifndef THORNBURY_JSON_PARSER_H_
#define THORNBURY_JSON_PARSER_H_

#if !defined (__THORNBURY_H_INSIDE__) && !defined (THORNBURY_COMPILATION)
#warning "Only <thornbury/thornbury.h> can be included directly."
#endif

#include <glib.h>
#include <yajl/yajl_parse.h>

G_BEGIN_DECLS

typedef enum _ThornburyRequestStatus
{
    THORNBURY_SUCCESS = 0,
    THORNBURY_ERR_FILE_OPEN_FAILED = 1,
    THORNBURY_ERR_INSUFFICIENT_MEMORY = 2,
    THORNBURY_ERR_YAJL_PARSE_FAILED = 3,
    THORNBURY_ERR_INVALID_PARAMS = 4,
    THORNBURY_ERR_UNKNOWN = 5
} ThornburyRequestStatus;

ThornburyRequestStatus ThornburyParseJsonFile (const gchar *filepath,
                                               yajl_callbacks* callbacks,
                                               gpointer ctx);
ThornburyRequestStatus ThornburyParseJsonResource (const gchar *path, const yajl_callbacks *callbacks, gpointer ctx, GError **error);

ThornburyRequestStatus ThornburyParseJsonBuffer (const guchar* buffer,
                                                 gsize buf_len,
                                                 const yajl_callbacks* callbacks,
                                                 gpointer ctx);

G_END_DECLS

#endif /* THORNBURY_JSON_PARSER_H_ */
