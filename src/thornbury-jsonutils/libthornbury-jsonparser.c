/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */



#include <stdio.h>
#include <stdlib.h>

#include <yajl/yajl_common.h>
#include <yajl/yajl_parse.h>
#include <yajl/yajl_gen.h>

#include "libthornbury-jsonparser.h"
#include <gio/gio.h>

#define MAX_BUFFER_SIZE     (65536)

/**
 * ThornburyParseJsonResource:
 * @path:Gresource path of view json file
 * @callbacks: a yajl callbacks structure specifying the functions to
 * call for given JSON entity.
 * @ctx: A context pointer that will be passed to callbacks.
 *
 * Returns: The end status of parsing the view json file.
 */
ThornburyRequestStatus
ThornburyParseJsonResource (const char *path,
                            const yajl_callbacks *callbacks,
                            gpointer ctx,
                            GError **error)
{
    const gchar *file_data;
    gsize read_size = 0;
    GBytes *resource = NULL;
    GError *tmp_error = NULL;
    ThornburyRequestStatus enResult;

    g_return_val_if_fail (path != NULL, THORNBURY_ERR_INVALID_PARAMS);
    g_return_val_if_fail (callbacks != NULL, THORNBURY_ERR_INVALID_PARAMS);
    g_return_val_if_fail (error == NULL || *error == NULL, THORNBURY_ERR_UNKNOWN);

    /* In global scope finding a given resource */
    resource = g_resources_lookup_data (path, G_RESOURCE_LOOKUP_FLAGS_NONE, &tmp_error);

    if (tmp_error != NULL)
    {
	g_debug ("JSONPARSER: error %s", tmp_error->message);
	g_propagate_error (error, tmp_error);
	return THORNBURY_ERR_UNKNOWN;
    }

    file_data = g_bytes_get_data (resource, &read_size);
    g_debug ("JSONPARSER: resource is  %s", file_data);
    enResult = ThornburyParseJsonBuffer (file_data, read_size, callbacks, ctx);

    g_bytes_unref (resource);
    return enResult;
}

/**
 * ThornburyParseJsonFile:
 * @pcFilePath:FilePath of view json file
 * @pCallbacks:Array of callback 
 * @ctx:data sructure.
 *
 * Returns: status. 
 */

ThornburyRequestStatus
ThornburyParseJsonFile (const gchar *pcFilePath,
                        yajl_callbacks *pCallbacks,
                        gpointer ctx)
{
    FILE *fp;
    void *pFileData;
    long lFileSize = 0;
    long lReadSize = 0;
    ThornburyRequestStatus enResult;

    if(NULL == pcFilePath || NULL == pCallbacks)
    {
        return THORNBURY_ERR_INVALID_PARAMS;
    }

    /* read json file */
    fp = fopen (pcFilePath, "r");
    if(fp == NULL)
	{
        printf("Error: Json file not found!\n");
		return THORNBURY_ERR_FILE_OPEN_FAILED;
	}

    /* find the file size */
	fseek(fp, 0, SEEK_END);
	lFileSize = ftell(fp);

    if(lFileSize > MAX_BUFFER_SIZE)
    {
        printf("Error: Insufficient buffer size! Increase limit!\n");
        fclose(fp);
        return THORNBURY_ERR_INSUFFICIENT_MEMORY;
    }

    pFileData = malloc (lFileSize);

    printf("File Size (read): %ld\n", lFileSize);
    fseek(fp, 0, SEEK_SET);
    lReadSize = fread((void *) pFileData, 1, lFileSize, fp);
    fclose(fp);
    fp = NULL;

    enResult = ThornburyParseJsonBuffer (pFileData, lReadSize, pCallbacks , ctx);

    free(pFileData);
    pFileData = NULL;

    return enResult;
}

/**
 * ThornburyParseJsonFile:
 * Returns: status. 
 */

ThornburyRequestStatus
ThornburyParseJsonBuffer (const guchar *pcBuffer,
                          gsize lBufLength,
                          const yajl_callbacks *pCallbacks,
                          gpointer ctx)
{
    yajl_handle hand;
    yajl_status stat;

    if( NULL == pcBuffer
        ||
        0 == lBufLength
        ||
        NULL == pCallbacks
      )
    {
        return THORNBURY_ERR_INVALID_PARAMS;
    }

    /* allow comments, utf8 chars */
    //yajl_parser_config cfg = { 1, 1 };

    /* Create Yajl handle */
    hand = yajl_alloc(pCallbacks, /*&cfg,*/ NULL, ctx);


    /* Start parsing the buffer */
    stat = yajl_parse(hand, pcBuffer, (size_t)lBufLength);

    yajl_free(hand);

    /*if (stat != yajl_status_ok &&
        stat != yajl_status_insufficient_data) */
    if (stat != yajl_status_ok)
    {
        /*unsigned char * str = yajl_get_error(hand, 1, fileData, rd);
        fprintf(stderr, (const char *) str);
        yajl_free_error(hand, str);*/
        return THORNBURY_ERR_UNKNOWN;
    }

    return THORNBURY_SUCCESS;
}



