m4_define([thornbury_major], [0])
m4_define([thornbury_minor], [2020])
m4_define([thornbury_micro], [1])
m4_define([thornbury_nano], [0])

m4_define([thornbury_base_version],
          [thornbury_major.thornbury_minor.thornbury_micro])

m4_define([thornbury_version],
          [m4_if(thornbury_nano, 0,
                [thornbury_base_version],
                [thornbury_base_version].[thornbury_nano])])

# Before making a release, the version info should be modified.
# Follow these instructions sequentially:
#   1. If the library source code has changed at all since the last update, then
#      increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
#   2. If any interfaces have been added, removed, or changed since the last
#      update, increment current, and set revision to 0.
#   3. If any interfaces have been added since the last public release, then
#      increment age.
#   4. If any interfaces have been removed or changed since the last public
#      release, then set age to 0. Also increment thornbury_major.
m4_define([thornbury_lt_current], [0])
m4_define([thornbury_lt_revision], [6])
m4_define([thornbury_lt_age], [0])

AC_PREREQ([2.62])
AC_INIT([thornbury], thornbury_version)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AM_MAINTAINER_MODE([enable])

AC_SUBST([THORNBURY_LT_CURRENT], thornbury_lt_current)
AC_SUBST([THORNBURY_LT_REVISION], thornbury_lt_revision)
AC_SUBST([THORNBURY_LT_AGE], thornbury_lt_age)
# Incremented if the API has incompatible changes
AC_SUBST([THORNBURY_API_VERSION], thornbury_major)
AC_SUBST(THORNBURY_VERSION,thornbury_version)

AM_INIT_AUTOMAKE([-Wno-portability subdir-objects tar-ustar])
AM_SILENT_RULES([yes])

AC_PROG_CC
AC_PROG_CXX
AC_ISC_POSIX
AC_STDC_HEADERS

LT_INIT

dnl Documentation
HOTDOC_CHECK([0.8], [c])

dnl check for gobject-introspection
GOBJECT_INTROSPECTION_CHECK([1.31.1])

THORNBURY_PACKAGES_PUBLIC_GLIB="glib-2.0"
THORNBURY_PACKAGES_PRIVATE_GLIB="gthread-2.0 gmodule-2.0 gio-unix-2.0 gio-2.0"

THORNBURY_PACKAGES_PUBLIC_SQLITE3=""
THORNBURY_PACKAGES_PRIVATE_SQLITE3="sqlite3"

THORNBURY_PACKAGES_PUBLIC_CLUTTER="clutter-1.0 >= 1.18.2"
THORNBURY_PACKAGES_PRIVATE_CLUTTER=""

THORNBURY_PACKAGES_PUBLIC_MX=""
THORNBURY_PACKAGES_PRIVATE_MX="mx-2.0 >= 1.99.4"

THORNBURY_PACKAGES_PUBLIC_PERSISTENTDATA=""
THORNBURY_PACKAGES_PRIVATE_PERSISTENTDATA="libseaton"

THORNBURY_PACKAGES_PUBLIC_APPMANAGER=""
THORNBURY_PACKAGES_PRIVATE_APPMANAGER="canterbury-gdbus-0"

THORNBURY_PACKAGES_PUBLIC_CURL=""
THORNBURY_PACKAGES_PRIVATE_CURL="libcurl"

THORNBURY_PACKAGES_PUBLIC_SOUP=""
THORNBURY_PACKAGES_PRIVATE_SOUP="libsoup-2.4"

THORNBURY_PACKAGES_PUBLIC_GDK_PIXBUF=""
THORNBURY_PACKAGES_PRIVATE_GDK_PIXBUF="gdk-pixbuf-2.0"

THORNBURY_PACKAGES_PUBLIC_JSON_GLIB=""
THORNBURY_PACKAGES_PRIVATE_JSON_GLIB="json-glib-1.0"

THORNBURY_PACKAGES_PUBLIC_YAJL=""
THORNBURY_PACKAGES_PRIVATE_YAJL="yajl"

THORNBURY_PACKAGES_PUBLIC_GTK=""
THORNBURY_PACKAGES_PRIVATE_GTK="gtk+-3.0"

THORNBURY_PACKAGES_PUBLIC="$THORNBURY_PACKAGES_PUBLIC_GLIB $THORNBURY_PACKAGES_PUBLIC_SQLITE3 $THORNBURY_PACKAGES_PUBLIC_CLUTTER $THORNBURY_PACKAGES_PUBLIC_MX $THORNBURY_PACKAGES_PUBLIC_PERSISTENTDATA $THORNBURY_PACKAGES_PUBLIC_APPMANAGER $THORNBURY_PACKAGES_PRIVATE_CURL $THORNBURY_PACKAGES_PRIVATE_SOUP $THORNBURY_PACKAGES_PRIVATE_GDK_PIXBUF $THORNBURY_PACKAGES_PRIVATE_JSON_GLIB $THORNBURY_PACKAGES_PRIVATE_YAJL $THORNBURY_PACKAGES_PRIVATE_GTK"
THORNBURY_PACKAGES_PRIVATE="$THORNBURY_PACKAGES_PRIVATE_GLIB $THORNBURY_PACKAGES_PRIVATE_SQLITE3 $THORNBURY_PACKAGES_PRIVATE_CLUTTER $THORNBURY_PACKAGES_PRIVATE_MX $THORNBURY_PACKAGES_PRIVATE_PERSISTENTDATA $THORNBURY_PACKAGES_PRIVATE_APPMANAGER $THORNBURY_PACKAGES_PRIVATE_CURL $THORNBURY_PACKAGES_PRIVATE_SOUP $THORNBURY_PACKAGES_PRIVATE_GDK_PIXBUF $THORNBURY_PACKAGES_PRIVATE_JSON_GLIB $THORNBURY_PACKAGES_PRIVATE_YAJL $THORNBURY_PACKAGES_PRIVATE_GTK"

AC_SUBST([THORNBURY_PACKAGES_PUBLIC])
AC_SUBST([THORNBURY_PACKAGES_PRIVATE])

PKG_CHECK_MODULES([GLIB], [$THORNBURY_PACKAGES_PUBLIC_GLIB $THORNBURY_PACKAGES_PRIVATE_GLIB])
PKG_CHECK_MODULES([SQLITE3], [$THORNBURY_PACKAGES_PUBLIC_SQLITE3 $THORNBURY_PACKAGES_PRIVATE_SQLITE3])
PKG_CHECK_MODULES([CLUTTER], [$THORNBURY_PACKAGES_PUBLIC_CLUTTER $THORNBURY_PACKAGES_PRIVATE_CLUTTER])
PKG_CHECK_MODULES([MX], [$THORNBURY_PACKAGES_PUBLIC_MX $THORNBURY_PACKAGES_PRIVATE_MX])
PKG_CHECK_MODULES([PERSISTENTDATA], [$THORNBURY_PACKAGES_PUBLIC_PERSISTENTDATA $THORNBURY_PACKAGES_PRIVATE_PERSISTENTDATA])
PKG_CHECK_MODULES([APPMANAGER], [$THORNBURY_PACKAGES_PUBLIC_APPMANAGER $THORNBURY_PACKAGES_PRIVATE_APPMANAGER])
PKG_CHECK_MODULES([CURL], [$THORNBURY_PACKAGES_PUBLIC_CURL $THORNBURY_PACKAGES_PRIVATE_CURL])
PKG_CHECK_MODULES([SOUP], [$THORNBURY_PACKAGES_PUBLIC_SOUP $THORNBURY_PACKAGES_PRIVATE_SOUP])
PKG_CHECK_MODULES([GDK_PIXBUF], [$THORNBURY_PACKAGES_PUBLIC_GDK_PIXBUF $THORNBURY_PACKAGES_PRIVATE_GDK_PIXBUF])
PKG_CHECK_MODULES([JSON_GLIB], [$THORNBURY_PACKAGES_PUBLIC_JSON_GLIB $THORNBURY_PACKAGES_PRIVATE_JSON_GLIB])
PKG_CHECK_MODULES([YAJL], [$THORNBURY_PACKAGES_PUBLIC_YAJL $THORNBURY_PACKAGES_PRIVATE_YAJL])
PKG_CHECK_MODULES([GTK], [$THORNBURY_PACKAGES_PUBLIC_GTK $THORNBURY_PACKAGES_PRIVATE_GTK])


#AX_PKG_CHECK_MODULES([GLIB],[glib-2.0],[gthread-2.0 gmodule-2.0 gio-unix-2.0 gio-2.0])
#AX_PKG_CHECK_MODULES([SQLITE3],[],[sqlite3])
#AX_PKG_CHECK_MODULES([CLUTTER],[clutter-1.0 >= 1.18.2],[])
#AX_PKG_CHECK_MODULES([CLUTTER_GTK],[clutter-gtk-1.0],[])
#AX_PKG_CHECK_MODULES([MX],[],[mx-2.0 >= 1.99.4])
#AX_PKG_CHECK_MODULES([PERSISTENTDATA],[],[libseaton])
#AX_PKG_CHECK_MODULES([APPMANAGER],[],[canterbury-gdbus-0])
#AX_PKG_CHECK_MODULES([CURL],[],[libcurl])
#AX_PKG_CHECK_MODULES([SOUP],[],[libsoup-2.4])
#AX_PKG_CHECK_MODULES([GDK_PIXBUF],[],[gdk-pixbuf-2.0])
#AX_PKG_CHECK_MODULES([JSON_GLIB],[],[json-glib-1.0])
#AX_PKG_CHECK_MODULES([YAJL],[],[yajl])
#AX_PKG_CHECK_MODULES([GTK],[],[gtk+-3.0])

GLIB_GSETTINGS

AX_COMPILER_FLAGS([], [],
                  dnl TODO: fix the warnings and change this from [yes] to
                  dnl [m4_if(thornbury_nano, 0, [yes], [no])],
                  [yes])

# Installed tests
AC_ARG_ENABLE(modular_tests,
         AS_HELP_STRING([--disable-modular-tests],
         [Disable build of test programs (default: no)]),,
         [enable_modular_tests=yes])

AC_ARG_ENABLE(installed_tests,
         AS_HELP_STRING([--enable-installed-tests],
         [Install test programs (default: no)]),,
         [enable_installed_tests=no])

AM_CONDITIONAL(BUILD_MODULAR_TESTS,
         [test "$enable_modular_tests" = "yes" ||
         test "$enable_installed_tests" = "yes"])
AM_CONDITIONAL(BUILDOPT_INSTALL_TESTS, test "$enable_installed_tests" = "yes")

# code coverage
AX_CODE_COVERAGE

# Walbottle support for JSON testing
AC_PATH_PROG([JSON_SCHEMA_VALIDATE],[json-schema-validate])
AC_PATH_PROG([JSON_SCHEMA_GENERATE],[json-schema-generate])

AS_IF([test "$JSON_SCHEMA_VALIDATE" = ""],
      [AC_MSG_ERROR([json-schema-validate not found])])
AS_IF([test "$JSON_SCHEMA_GENERATE" = ""],
      [AC_MSG_ERROR([json-schema-generate not found])])

AC_CONFIG_FILES([
    Makefile
    docs/Makefile
    docs/reference/Makefile
    src/Makefile
    tests/Makefile
    scripts/Makefile
    libthornbury.pc
    libthornbury-]thornbury_major[.pc
])

AC_OUTPUT

dnl Summary
echo "    thornbury"
echo "    --------------"
echo "    Documentation                  : ${enable_documentation}"
echo "    code coverage                  : ${enable_code_coverage}"
echo "    compiler warings               : ${enable_compile_warnings}"
echo "    Test suite                     : ${enable_modular_tests}"
echo "    Introspection                  : ${found_introspection}"
