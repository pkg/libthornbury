## Libthornbury Overview

libthornbury is a group of user Interface utility libraries.

[Mail the maintainers](mailto:libthornbury@apertis.org)

### Brief introduction to thornbury.

Thornbury provides the helper function for customized json parsing which is used in view manager, texture creation for static .png files or download from the web sychronously/asynchronously using [`libsoup`](#libsoup) to create the texture using [`GdkPixbuf`](#GdkPixbuf),View manager is intended to create/manage the views in an app using [`Clutter`](#Clutter), ItemFactory is used to instantiate any given widget type which implements [`MxItemfactory`](#MxItemfactory). Model to give the MVC approach to the app inspired by [`ClutterModel`](#ClutterModel).

[`LINK`](#LINK:CAPS) and write some more discription

See Also, or External Links or Further References like link to
